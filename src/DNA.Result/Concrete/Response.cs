﻿using DNA.Result.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Result.Concrete
{
    public class Response : IResponse
    {
        public Response(bool success, string message) : this(success)
        {
            this.Message = message;
        }

        public Response(bool success)
        {
            this.Success = success;
        }

        public bool Success { get; }

        public string Message { get; }

    }
}
