﻿using DNA.Result.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Result.Concrete
{
    public class ResponseData<T> : Response, IResponseData<T>
    {
        public ResponseData(T data, bool success, string message) : base(success, message)
        {
            this.Data = data;
        }

        public ResponseData(T data, bool success) : base(success)
        {
            this.Data = data;
        }

        public T Data { get; }

    }
}
