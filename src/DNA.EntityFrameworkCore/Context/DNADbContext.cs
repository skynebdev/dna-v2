﻿using DNA.Domain.Models;
using DNA.Domain.Models.ClientProducts;
using DNA.Domain.Models.Comments;
using DNA.Domain.Models.Posts;
using DNA.Domain.Models.Products;
using DNA.Domain.Models.Symbols;
using DNA.Domain.Models.UserSymbols;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.EntityFrameworkCore.Context
{
    public class DNADbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Symbol> Symbols { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ClientProduct> ClientProducts { get; set; }
        public DbSet<UserSymbol> UserSymbols { get; set; }
        public DbSet<CommentPoint> CommentPoints { get; set; }
        public DbSet<PinnedComment> PinnedComments { get; set; }
        public DbSet<PinnedPost> PinnedPosts { get; set; }
        
        public DNADbContext(DbContextOptions<DNADbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}
