﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DNA.EntityFrameworkCore.Migrations
{
    public partial class post_viewers_added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Viewers",
                table: "Posts",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Viewers",
                table: "Posts");
        }
    }
}
