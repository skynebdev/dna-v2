﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DNA.EntityFrameworkCore.Migrations
{
    public partial class comment_point_added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "Point",
                table: "Comments",
                type: "double",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "PointCount",
                table: "Comments",
                type: "double",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Point",
                table: "Comments");

            migrationBuilder.DropColumn(
                name: "PointCount",
                table: "Comments");
        }
    }
}
