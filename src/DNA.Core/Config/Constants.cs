﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Core.Config
{
    public static class Constants
    {
        #region Error Messages
        public const string inputForbiddenWordError = "The values ​​you entered contain forbidden words";
        public const string emptyInput = "Please enter all values ​​completely!";
        public const string unauthorizedError = "401 Unauthorized";

        public const string registerError = "Something gones wrong while registering!";
        public const string passwordConfirmError = "Passwords did not match!";
        public const string mobileUsedError = "This mobile number is used before!";
        public const string emailUsedError = "This email is used before!";
        public const string loginError = "Email or password is wrong";

        public const string symboLCreateError = "Something gones wrong while symbol creating!";
        public const string symbolUpdateError = "Something gones wrong while symbol updating!";
        public const string symbolDeletedError = "Something gones wrong while symbol deleting!";
        public const string symbolNotFoundError = "Symbol Not Found!";

        public const string postCreateError = "Something gones wrong while post creating!";
        public const string postUpdateError = "Something gones wrong while post updating!";
        public const string postDeleteError = "Something gones wrong while post updating!";
        public const string postNotFoundError = "Post Not Found!";

        public const string commentCreateError = "Something gones wrong while comment creating!";
        public const string commentUpdateError = "Something gones wrong while comment updating!";
        public const string commentDeleteError = "Something gones wrong while comment deleting!";
        public const string commentNotFoundError = "Comment Not Found!";

        public const string productCreateError = "Something gones wrong while product creating!";
        public const string productUpdateError = "Something gones wrong while product updating!";
        public const string productDeleteError = "Something gones wrong while product deleting!";
        public const string productNotFoundError = "Product Not Found!";

        public const string clientProductCreateError = "Something gones wrong while product buying!";
        public const string clientProductSetError = "Something gones wrong while product matching with client!";
        public const string alreadyPendingRequestError = "You already have a pending application!";
        public const string freeTrialUsedBefore = "You already have been used free trial!";
        public const string freeTrialCreateError = "Something gones wrong while free trial matching with client!";

        public const string productNotBuyed = "In order to benefit from this service, you need to purchase the product.";
        public const string pointedError = "Something gones wrong while pointing!";
        public const string allreadyPointedError = "This comment pointed before!";

        public const string allreadyPinnedError = "This comment pinned before!";
        public const string pinnedError = "Something gones wrong while marking!";
        public const string unpinnedError = "Something gones wrong while unmarking!";

        #endregion

        #region Successful Messages
        public const string registerSuccessful = "Register successfuly!";

        public const string symbolCreateSuccessful = "Symbol created successfuly!";
        public const string symbolUpdateSuccessful = "Symbol updated successfuly!";
        public const string symbolDeletedSuccessful = "Symbol deleted successfuly!";

        public const string postCreateSuccessful = "Post created successfuly!";
        public const string postUpdateSuccessful = "Post updated successfuly!";
        public const string postDeleteSuccessful = "Post deleted successfuly!";

        public const string commentCreateSuccessful = "Comment created successfuly!";
        public const string commentUpdateSuccessful = "Comment updated successfuly!";
        public const string commentDeleteSuccessful = "Comment deleted successfuly!";

        public const string productCreateSuccessful = "Product created successfuly!";
        public const string productUpdateSuccessful = "Product updated successfuly!";
        public const string productDeleteSuccessful = "Product deleted successfuly!";

        public const string clientProductCreateSuccessful = "The product purchase request has been completed successfully, it is in the process of waiting for approval!";
        public const string clientProductSetSuccessful = "The product successfuly matched with client!";
        public const string freeTrialCreateSuccessful = "The free trial successfuly matched with client!";
        public const string pointedSuccessful = "The comment is successfuly pointed!";

        public const string pinnedSuccessful = "The comment is successfuly pinned!";
        public const string unpinnedSuccessful = "The comment is successfuly unpinned!";
        #endregion
    }
}
