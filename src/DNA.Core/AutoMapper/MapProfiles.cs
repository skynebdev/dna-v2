﻿using AutoMapper;
using DNA.Domain.Dtos.ClientProducts;
using DNA.Domain.Dtos.Comments;
using DNA.Domain.Dtos.Identity;
using DNA.Domain.Dtos.Posts;
using DNA.Domain.Dtos.Products;
using DNA.Domain.Dtos.Symbols;
using DNA.Domain.Dtos.UserSymbols;
using DNA.Domain.Models;
using DNA.Domain.Models.ClientProducts;
using DNA.Domain.Models.Comments;
using DNA.Domain.Models.Posts;
using DNA.Domain.Models.Products;
using DNA.Domain.Models.Symbols;
using DNA.Domain.Models.UserSymbols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Core.AutoMapper
{
    public class MapProfiles : Profile
    {
        public MapProfiles()
        {
            #region Identity Mappings
            CreateMap<RegisterDto, User>().ReverseMap();
            CreateMap<UserLoginInfoDto, User>().ReverseMap();
            CreateMap<UserLoginInfoDto, Admin>().ReverseMap();
            #endregion

            #region Symbol Mappings
            CreateMap<CreateSymbolDto, Symbol>().ReverseMap();
            CreateMap<UpdateSymbolDto, Symbol>().ReverseMap();
            CreateMap<DeleteSymbolDto, Symbol>().ReverseMap();
            #endregion

            #region Post Mappings
            CreateMap<CreatePostDto, Post>().ReverseMap();
            CreateMap<UpdatePostDto, Post>().ReverseMap();
            CreateMap<DeletePostDto, Post>().ReverseMap();
            #endregion

            #region Comment Mappings
            CreateMap<CreateCommentDto, Comment>().ReverseMap();
            CreateMap<UpdateCommentDto, Comment>().ReverseMap();
            CreateMap<DeleteCommentDto, Comment>().ReverseMap();
            #endregion

            #region Product Mappings
            CreateMap<CreateProductDto, Product>().ReverseMap();
            CreateMap<UpdateProductDto, Product>().ReverseMap();
            CreateMap<DeleteProductDto, Product>().ReverseMap();
            #endregion

            #region Client Products Mappings
            CreateMap<CreateClientProductDto, ClientProduct>().ReverseMap();
            #endregion

            #region User Symbol Mappings
            CreateMap<CreateUserSymbolDto, UserSymbol>().ReverseMap();
            #endregion

            #region Comment Points Mappings
            CreateMap<SetPointDto, CommentPoint>().ReverseMap();
            #endregion

            #region Pinned Comment Mappings
            CreateMap<PinCommentDto, PinnedComment>().ReverseMap();
            #endregion

            #region Pinned Post Mappings
            CreateMap<PinPostDto, PinnedPost>().ReverseMap();
            #endregion
        }
    }
}
