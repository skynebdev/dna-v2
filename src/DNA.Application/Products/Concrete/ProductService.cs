﻿using DNA.Application.Products.Abstract;
using DNA.Business.Products.Abstract;
using DNA.Core.Config;
using DNA.Domain.Dtos.Products;
using DNA.Domain.Models.Products;
using DNA.Repository.Products.Abstract;
using DNA.Result.Abstract;
using DNA.Result.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Application.Products.Concrete
{
    public class ProductService : IProductService
    {
        public readonly IProductManager _productManager;
        public readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository, IProductManager productManager)
        {
            _productRepository = productRepository;
            _productManager = productManager;
        }

        /// <summary>
        /// Create a new comment
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task<IResponse> Create(CreateProductDto comment)
        {
            var inputControls = _productManager.Create(comment); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var isCreated = await _productRepository.Create(comment);
            if (isCreated)
                return new Response(true, Constants.productCreateSuccessful);

            return new Response(false, Constants.productCreateError);
        }

        /// <summary>
        /// Delete comment by id
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task<IResponse> Delete(DeleteProductDto comment)
        {
            var inputControls = _productManager.Delete(comment); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var isDeleted = await _productRepository.Delete(comment);
            if (isDeleted)
                return new Response(true, Constants.productDeleteSuccessful);

            return new Response(false, Constants.productDeleteError);
        }

        /// <summary>
        /// Update comment by id
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task<IResponse> Update(UpdateProductDto comment)
        {
            var inputControls = _productManager.Update(comment); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var isCreated = await _productRepository.Update(comment);
            if (isCreated)
                return new Response(true, Constants.productUpdateSuccessful);

            return new Response(false, Constants.productUpdateError);
        }

        /// <summary>
        /// Returning all comments
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public IResponse AllProducts(Guid userId, bool type = true, string token = null)
        {
            var inputControls = _productManager.AllProducts(userId, type, token); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var allProducts = _productRepository.AllProducts();

            if (allProducts is not null && allProducts.Count > 0)
                return new ResponseData<List<Product>>(allProducts, true);

            return new Response(false, Constants.productNotFoundError);
        }

        /// <summary>
        /// Returning comment by id
        /// </summary>
        /// <param name="commentId"></param>
        /// <param name="userId"></param>
        /// <param name="token"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public IResponse ProductById(Guid productId, Guid userId, string token = null, bool type = true)
        {
            var inputControls = _productManager.ProductById(productId, userId, token, type); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var comment = _productRepository.ProductById(productId);

            if (comment is not null)
                return new ResponseData<Product>(comment, true);

            return new Response(false, Constants.productNotFoundError);
        }
    }
}
