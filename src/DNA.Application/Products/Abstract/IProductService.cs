﻿using DNA.Domain.Dtos.Products;
using DNA.Result.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Application.Products.Abstract
{
    public interface IProductService
    {
        Task<IResponse> Create(CreateProductDto product);
        Task<IResponse> Update(UpdateProductDto product);
        Task<IResponse> Delete(DeleteProductDto product);
        IResponse AllProducts(Guid userId, bool type = true, string token = null);
        IResponse ProductById(Guid productId, Guid userId, string token = null, bool type = true);
    }
}
