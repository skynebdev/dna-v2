﻿using DNA.Domain.Dtos.Posts;
using DNA.Result.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Application.Posts.Abstract
{
    public interface IPostService
    {
        Task<IResponse> Create(CreatePostDto post);
        Task<IResponse> Update(UpdatePostDto post);
        Task<IResponse> Delete(DeletePostDto post);
        IResponse AllPosts(Guid userId, bool type = true, string token = null);
        Task<IResponse> PostById(Guid postId, Guid userId, string token = null, bool type = true);
        Task<IResponse> PinPost(PinPostDto post);
        Task<IResponse> UnpinPost(PinPostDto post);
        IResponse PinnedPosts(Guid userId, string token = null);
    }
}
