﻿using DNA.Application.Posts.Abstract;
using DNA.Business.Posts.Abstract;
using DNA.Core.Config;
using DNA.Domain.Dtos.Posts;
using DNA.Domain.Models.Posts;
using DNA.Repository.Posts.Abstract;
using DNA.Result.Abstract;
using DNA.Result.Concrete;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Application.Posts.Concrete
{
    public class PostService : IPostService
    {
        public readonly IPostManager _postManager;
        public readonly IPostRepository _postRepository;

        public PostService(IPostRepository postRepository, IPostManager postManager)
        {
            _postRepository = postRepository;
            _postManager = postManager;
        }

        /// <summary>
        /// Creating a new post
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<IResponse> Create(CreatePostDto post)
        {
            var inputControls = _postManager.Create(post); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var isCreated = await _postRepository.Create(post);
            if (isCreated)
                return new Response(true, Constants.postCreateSuccessful);

            return new Response(false, Constants.postCreateError);
        }

        /// <summary>
        /// Updating a current post
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        public async Task<IResponse> Update(UpdatePostDto post)
        {
            var inputControls = _postManager.Update(post); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var isCreated = await _postRepository.Update(post);
            if (isCreated)
                return new Response(true, Constants.postUpdateSuccessful);

            return new Response(false, Constants.postUpdateError);
        }

        /// <summary>
        /// Delete post by id
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        public async Task<IResponse> Delete(DeletePostDto post)
        {
            var inputControls = _postManager.Delete(post); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var isCreated = await _postRepository.Delete(post);
            if (isCreated)
                return new Response(true, Constants.postDeleteSuccessful);

            return new Response(false, Constants.postDeleteError);
        }

        /// <summary>
        /// Get all posts
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <param name="token"></param>
        public IResponse AllPosts(Guid userId, bool type = true, string token = null)
        {
            var inputControls = _postManager.AllPosts(userId,type,token); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var allPosts = _postRepository.AllPosts();

            if (allPosts is not null && allPosts.Count > 0)
                return new ResponseData<List<Post>>(allPosts, true);

            return new Response(false, Constants.postNotFoundError);
        }

        /// <summary>
        /// Getting post by id
        /// </summary>
        /// <param name="postId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<IResponse> PostById(Guid postId, Guid userId, string token, bool type = true)
        {
            var inputControls = _postManager.PostById(postId, userId, token, type); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var post = _postRepository.PostById(postId);

            if (post is not null)
            {
                await _postRepository.IncreaseViews(postId);
                return new ResponseData<Post>(post, true);
            }

            return new Response(false, Constants.postNotFoundError);
        }

        /// <summary>
        /// Operations of pin comment
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task<IResponse> PinPost(PinPostDto post)
        {
            var inputControls = _postManager.PinPost(post); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var pinComment = await _postRepository.PinPost(post);

            if (pinComment)
                return new Response(true, Constants.pinnedSuccessful);

            return new Response(false, Constants.pinnedError);
        }

        /// <summary>
        /// Operations of pin comment
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task<IResponse> UnpinPost(PinPostDto post)
        {
            var inputControls = _postManager.UnpinPost(post); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var pinComment = await _postRepository.UnpinPost(post);

            if (pinComment)
                return new Response(true, Constants.unpinnedSuccessful);

            return new Response(false, Constants.unpinnedError);
        }

        /// <summary>
        /// Operations of pinned comments
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public IResponse PinnedPosts(Guid userId, string token = null)
        {
            var inputControls = _postManager.PinnedPosts(userId, token); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var posts = _postRepository.PinnedPosts(userId);

            if (posts is not null && posts.Count > 0)
                return new ResponseData<List<Post>>(posts, true);

            return new Response(false, Constants.postNotFoundError);
        }
    }
}
