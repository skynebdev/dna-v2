﻿using DNA.Application.UserSymbols.Abstract;
using DNA.Business.UserSymbols.Abstract;
using DNA.Core.Config;
using DNA.Domain.Dtos.UserSymbols;
using DNA.Repository.UserSymbols.Abstract;
using DNA.Result.Abstract;
using DNA.Result.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Application.UserSymbols.Concrete
{
    public class UserSymbolService : IUserSymbolService
    {
        public readonly IUserSymbolManager _userSymbolManager;
        public readonly IUserSymbolRepository _userSymbolRepository;

        public UserSymbolService(IUserSymbolRepository userSymbolRepository, IUserSymbolManager userSymbolManager)
        {
            _userSymbolRepository = userSymbolRepository;
            _userSymbolManager = userSymbolManager;
        }

        /// <summary>
        /// Create new symbol for user
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task<IResponse> Create(CreateUserSymbolDto symbol)
        {
            var inputControls = _userSymbolManager.Create(symbol); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var isCreated = await _userSymbolRepository.Create(symbol);
            if (isCreated)
                return new Response(true, Constants.symbolCreateSuccessful);

            return new Response(false, Constants.symboLCreateError);
        }

        /// <summary>
        /// Update symbol for user
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        public async Task<IResponse> Update(UpdateUserSymbolDto symbol)
        {
            var inputControls = _userSymbolManager.Update(symbol); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var isUpdated = await _userSymbolRepository.Update(symbol);
            if (isUpdated)
                return new Response(true, Constants.symbolUpdateSuccessful);

            return new Response(false, Constants.symbolUpdateError);
        }

        /// <summary>
        /// Get symbols of user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IResponse GetUserSymbols(Guid userId, string token)
        {
            var inputControls = _userSymbolManager.GetUserSymbols(userId, token); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var userSymbols = _userSymbolRepository.GetUserSymbols(userId);

            if (userSymbols is not null && userSymbols.Count > 0)
                return new ResponseData<List<GetUserSymbolsDto>>(userSymbols, true);

            return new Response(false, Constants.productNotFoundError);
        }

        /// <summary>
        /// Delete user symbol
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        public async Task<IResponse> Delete(DeleteUserSymbolDto symbol)
        {
            var inputControls = _userSymbolManager.Delete(symbol); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var isUpdated = await _userSymbolRepository.Delete(symbol);
            if (isUpdated)
                return new Response(true, Constants.symbolUpdateSuccessful);

            return new Response(false, Constants.symbolUpdateError);
        }
    }
}
