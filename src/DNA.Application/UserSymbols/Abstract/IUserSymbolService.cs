﻿using DNA.Domain.Dtos.UserSymbols;
using DNA.Result.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Application.UserSymbols.Abstract
{
    public interface IUserSymbolService
    {
        Task<IResponse> Create(CreateUserSymbolDto symbol);
        Task<IResponse> Update(UpdateUserSymbolDto symbol);
        IResponse GetUserSymbols(Guid userId, string token);
        Task<IResponse> Delete(DeleteUserSymbolDto symbol);
    }
}
