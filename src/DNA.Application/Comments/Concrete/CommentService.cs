﻿using DNA.Application.Comments.Abstract;
using DNA.Business.Comments.Abstract;
using DNA.Core.Config;
using DNA.Domain.Dtos.Comments;
using DNA.Domain.Models.Comments;
using DNA.Repository.Comments.Abstract;
using DNA.Result.Abstract;
using DNA.Result.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Application.Comments.Concrete
{
    public class CommentService : ICommentService
    {
        public readonly ICommentManager _commentManager;
        public readonly ICommentRepository _commentRepository;

        public CommentService(ICommentRepository commentRepository, ICommentManager commentManager)
        {
            _commentRepository = commentRepository;
            _commentManager = commentManager;
        }

        /// <summary>
        /// Create a new comment
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task<IResponse> Create(CreateCommentDto comment)
        {
            var inputControls = _commentManager.Create(comment); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var isCreated = await _commentRepository.Create(comment);
            if (isCreated)
                return new Response(true, Constants.commentCreateSuccessful);
            
            return new Response(false, Constants.commentCreateError);
        }

        /// <summary>
        /// Delete comment by id
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task<IResponse> Delete(DeleteCommentDto comment)
        {
            var inputControls = _commentManager.Delete(comment); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var isDeleted = await _commentRepository.Delete(comment);
            if (isDeleted)
                return new Response(true, Constants.commentDeleteSuccessful);

            return new Response(false, Constants.commentDeleteError);
        }

        /// <summary>
        /// Update comment by id
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task<IResponse> Update(UpdateCommentDto comment)
        {
            var inputControls = _commentManager.Update(comment); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var isCreated = await _commentRepository.Update(comment);
            if (isCreated)
                return new Response(true, Constants.commentUpdateSuccessful);

            return new Response(false, Constants.commentUpdateError);
        }

        /// <summary>
        /// Returning all comments
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public IResponse AllComments(Guid userId, bool type = true, string token = null)
        {
            var inputControls = _commentManager.AllComments(userId, type, token); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var allComments = _commentRepository.AllComments();

            if (allComments is not null && allComments.Count > 0)
                return new ResponseData<List<Comment>>(allComments, true);

            return new Response(false, Constants.commentNotFoundError);
        }

        /// <summary>
        /// Returning comment by id
        /// </summary>
        /// <param name="commentId"></param>
        /// <param name="userId"></param>
        /// <param name="token"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public IResponse CommentById(Guid commentId, Guid userId, string token = null, bool type = true)
        {
            var inputControls = _commentManager.CommentById(commentId, userId, token, type); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var comment = _commentRepository.CommentById(commentId);

            if (comment is not null)
                return new ResponseData<Comment>(comment, true);

            return new Response(false, Constants.commentNotFoundError);
        }

        /// <summary>
        /// Set point to comment
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public async Task<IResponse> SetPoint(SetPointDto point)
        {
            var inputControls = _commentManager.SetPoint(point); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var setPoint = await _commentRepository.SetPoint(point);
            if (setPoint)
                return new Response(true, Constants.pointedSuccessful);

            return new Response(false, Constants.pointedError);
        }

        /// <summary>
        /// Operations of pin comment
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task<IResponse> PinComment(PinCommentDto comment)
        {
            var inputControls = _commentManager.PinComment(comment); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var pinComment = await _commentRepository.PinComment(comment);

            if (pinComment)
                return new Response(true,Constants.pinnedSuccessful);

            return new Response(false, Constants.pinnedError);
        }

        /// <summary>
        /// Operations of pin comment
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task<IResponse> UnpinComment(PinCommentDto comment)
        {
            var inputControls = _commentManager.UnpinComment(comment); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var pinComment = await _commentRepository.UnpinComment(comment);

            if (pinComment)
                return new Response(true, Constants.unpinnedSuccessful);

            return new Response(false, Constants.unpinnedError);
        }

        /// <summary>
        /// Operations of pinned comments
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public IResponse PinnedComments(Guid userId, string token = null)
        {
            var inputControls = _commentManager.PinnedComments(userId, token); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var comment = _commentRepository.PinnedComments(userId);

            if (comment is not null && comment.Count > 0)
                return new ResponseData<List<Comment>>(comment, true);

            return new Response(false, Constants.commentNotFoundError);
        }
    }
}
