﻿using DNA.Domain.Dtos.Comments;
using DNA.Result.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Application.Comments.Abstract
{
    public interface ICommentService
    {
        Task<IResponse> Create(CreateCommentDto comment);
        Task<IResponse> Update(UpdateCommentDto comment);
        Task<IResponse> Delete(DeleteCommentDto comment);
        IResponse AllComments(Guid userId, bool type = true, string token = null);
        IResponse CommentById(Guid commentId, Guid userId, string token = null, bool type = true);
        Task<IResponse> SetPoint(SetPointDto point);
        Task<IResponse> PinComment(PinCommentDto comment);
        Task<IResponse> UnpinComment(PinCommentDto comment);
        IResponse PinnedComments(Guid userId, string token = null);

    }
}
