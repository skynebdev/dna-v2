﻿using DNA.Application.ClientProducts.Abstract;
using DNA.Business.ClientProducts.Abstract;
using DNA.Core.Config;
using DNA.Domain.Dtos.ClientProducts;
using DNA.Domain.Models.ClientProducts;
using DNA.Repository.ClientProducts.Abstract;
using DNA.Result.Abstract;
using DNA.Result.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Application.ClientProducts.Concrete
{
    public class ClientProductService : IClientProductService
    {
        public readonly IClientProductsManager _clientProductManager;
        public readonly IClientProductsRepository _clientProductRepository;

        public ClientProductService(IClientProductsRepository clientProductRepository, IClientProductsManager clientProductManager)
        {
            _clientProductRepository = clientProductRepository;
            _clientProductManager = clientProductManager;
        }

        /// <summary>
        /// Create a new client product
        /// </summary>
        /// <param name="clientProduct"></param>
        /// <returns></returns>
        public async Task<IResponse> Create(CreateClientProductDto clientProduct)
        {
            var inputControls = _clientProductManager.Create(clientProduct); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var isCreated = await _clientProductRepository.Create(clientProduct);
            if (isCreated)
                return new Response(true, Constants.clientProductCreateSuccessful);

            return new Response(false, Constants.clientProductCreateError);
        }

        /// <summary>
        /// Set product to waiting customer
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public async Task<IResponse> SetProductToCustomer(SetProductToCustomerDto product)
        {
            var inputControls = _clientProductManager.SetProductToCustomer(product); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var productPeriod = _clientProductRepository.GetProductPeriod(product.ProductId);
            var userProductExpireDate = _clientProductRepository.GetClientProductExpireDate(product.Id, product.UserId);

            product.ExpiredDate = userProductExpireDate.AddMonths(productPeriod);

            var isCreated = await _clientProductRepository.SetProductToCustomer(product);
            if (isCreated)
                return new Response(true, Constants.clientProductSetSuccessful);

            return new Response(false, Constants.clientProductSetError);
        }

        /// <summary>
        /// Returned all approval pending users
        /// </summary>
        /// <param name="adminId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public IResponse GetUsersPendingApproval(Guid adminId, string token)
        {
            var inputControls = _clientProductManager.GetUsersPendingApproval(adminId, token); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var allPendingApprovals = _clientProductRepository.GetUsersPendingApproval();

            if (allPendingApprovals is not null && allPendingApprovals.Count > 0)
                return new ResponseData<List<PendingApprovalsDto>>(allPendingApprovals, true);

            return new Response(false, Constants.productNotFoundError);
        }

        /// <summary>
        /// Returned client producst by client id
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public IResponse GetClientProducts(Guid userId, bool type, string token)
        {
            var inputControls = _clientProductManager.GetClientProducts(userId, type, token); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var clientProducts = _clientProductRepository.GetClientProducts(userId);
            if (clientProducts is not null && clientProducts.Products.Count > 0)
                return new ResponseData<ClientProductByClientDto>(clientProducts, true);

            return new Response(false, Constants.productNotFoundError);
        }

        /// <summary>
        /// Returned all client producst
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public IResponse GetAllClientProducts(Guid adminId, string token)
        {
            var inputControls = _clientProductManager.GetAllClientProducts(adminId, token); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var clientProducts = _clientProductRepository.GetAllClientProducts();
            if (clientProducts is not null && clientProducts.Count > 0)
                return new ResponseData<List<ClientAllProductsDto>>(clientProducts, true);

            return new Response(false, Constants.productNotFoundError);
        }

        /// <summary>
        /// Set free trial to user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="adminId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<IResponse> SetFreeTrial(Guid userId, string token)
        {
            var inputControls = _clientProductManager.SetFreeTrial(userId, token); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var isCreated = await _clientProductRepository.SetFreeTrial(userId);
            if (isCreated)
                return new Response(true, Constants.freeTrialCreateSuccessful);

            return new Response(false, Constants.freeTrialCreateError);
        }
    }
}
