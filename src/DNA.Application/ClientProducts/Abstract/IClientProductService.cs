﻿using DNA.Domain.Dtos.ClientProducts;
using DNA.Result.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Application.ClientProducts.Abstract
{
    public interface IClientProductService
    {
        Task<IResponse> Create(CreateClientProductDto clientProduct);
        Task<IResponse> SetProductToCustomer(SetProductToCustomerDto product);
        IResponse GetUsersPendingApproval(Guid adminId, string token);
        IResponse GetClientProducts(Guid userId, bool type, string token);
        IResponse GetAllClientProducts(Guid adminId, string token);
        Task<IResponse> SetFreeTrial(Guid userId, string token);
    }
}
