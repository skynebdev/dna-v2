﻿using DNA.Domain.Dtos.Symbols;
using DNA.Result.Abstract;
using System.Threading.Tasks;

namespace DNA.Application.Symbols.Abstract
{
    public interface ISymbolService
    {
        Task<IResponse> Create(CreateSymbolDto symbol);
        Task<IResponse> Update(UpdateSymbolDto symbol);
        Task<IResponse> Delete(DeleteSymbolDto symbol);
        IResponse AllSymbols();
    }
}
