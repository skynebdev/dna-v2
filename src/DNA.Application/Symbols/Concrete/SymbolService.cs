﻿using DNA.Application.Symbols.Abstract;
using DNA.Business.Symbols.Abstract;
using DNA.Core.Config;
using DNA.Domain.Dtos.Symbols;
using DNA.Domain.Models.Symbols;
using DNA.Repository.Symbols.Abstract;
using DNA.Result.Abstract;
using DNA.Result.Concrete;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Application.Symbols.Concrete
{
    public class SymbolService : ISymbolService
    {
        public readonly ISymbolManager _symbolManager;
        public readonly ISymbolRepository _symbolRepository;

        public SymbolService(ISymbolRepository symbolRepository, ISymbolManager symbolManager)
        {
            _symbolRepository = symbolRepository;
            _symbolManager = symbolManager;
        }

        /// <summary>
        /// Creating new symbol
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        public async Task<IResponse> Create(CreateSymbolDto symbol)
        {
            var inputControls = _symbolManager.Create(symbol); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var isCreated = await _symbolRepository.Create(symbol);
            if (isCreated)
                return new Response(true, Constants.symbolCreateSuccessful);

            return new Response(false, Constants.symboLCreateError);
        }

        /// <summary>
        /// Update a current symbol
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        public async Task<IResponse> Update(UpdateSymbolDto symbol)
        {
            var inputControls = _symbolManager.Update(symbol); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var isCreated = await _symbolRepository.Update(symbol);
            if (isCreated)
                return new Response(true, Constants.symbolUpdateSuccessful);

            return new Response(false, Constants.symbolUpdateError);
        }

        /// <summary>
        /// Delete a current symbol
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        public async Task<IResponse> Delete(DeleteSymbolDto symbol)
        {
            var inputControls = _symbolManager.Delete(symbol); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var isCreated = await _symbolRepository.Delete(symbol);
            if (isCreated)
                return new Response(true, Constants.symbolDeletedSuccessful);

            return new Response(false, Constants.symbolDeletedError);
        }

        /// <summary>
        /// Returned all symbols
        /// </summary>
        /// <returns></returns>
        public IResponse AllSymbols()
        {
            var allSymbols = _symbolRepository.AllSymbols();

            if (allSymbols is not null && allSymbols.Count > 0)
                return new ResponseData<List<Symbol>>(allSymbols,true);

            return new Response(false, Constants.symbolNotFoundError);
        }
    }
}
