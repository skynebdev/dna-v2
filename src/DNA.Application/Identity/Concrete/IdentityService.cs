﻿using DNA.Application.Identity.Abstract;
using DNA.Business.Identity.Abstract;
using DNA.Core.Config;
using DNA.Domain.Dtos.Identity;
using DNA.Repository.Identity.Abstract;
using DNA.Result.Abstract;
using DNA.Result.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Application.Identity.Concrete
{
    public class IdentityService : IIdentityService
    {
        public readonly IIdentityManager _identityManager;
        public readonly IIdentityRepository _identityRepository;

        public IdentityService(IIdentityRepository identityRepository, IIdentityManager identityManager)
        {
            _identityRepository = identityRepository;
            _identityManager = identityManager;
        }

        /// <summary>
        /// Register a new user
        /// </summary>
        /// <param name="register"></param>
        /// <returns></returns>
        public async Task<IResponse> Register(RegisterDto register)
        {
            var inputControls = _identityManager.Register(register); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var userRegister = await _identityRepository.Register(register);
            if (userRegister)
                return new Response(true, Constants.registerSuccessful);

            return new Response(false, Constants.registerError);
        }

        /// <summary>
        /// User login with email and password
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public async Task<IResponse> Login(LoginDto login)
        {
            var inputControls = _identityManager.Login(login); // Controls at business layer
            if (!inputControls.Success) // If somethings gones wrong return error and result
                return inputControls;

            var userLogin = await _identityRepository.Login(login);
            if (userLogin is null)
                return new Response(false, Constants.loginError);

            return new ResponseData<UserLoginInfoDto>(userLogin, true);
        }

    }
}
