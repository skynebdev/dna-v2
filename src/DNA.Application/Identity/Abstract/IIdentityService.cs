﻿using DNA.Domain.Dtos.Identity;
using DNA.Result.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Application.Identity.Abstract
{
    public interface IIdentityService
    {
        Task<IResponse> Register(RegisterDto register);
        Task<IResponse> Login(LoginDto login);

    }
}
