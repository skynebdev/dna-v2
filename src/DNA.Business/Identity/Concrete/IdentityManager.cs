﻿using DNA.Business.Identity.Abstract;
using DNA.Core.Config;
using DNA.Core.Helpers;
using DNA.Domain.Dtos.Identity;
using DNA.Repository.Identity.Abstract;
using DNA.Repository.Identity.Authentication.Abstract;
using DNA.Result.Abstract;
using DNA.Result.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Business.Identity.Concrete
{
    public class IdentityManager : IIdentityManager
    {
        private readonly ITokenOperations _tokenOperations;
        private readonly IIdentityRepository _identityRepository;

        public IdentityManager(ITokenOperations tokenOperations, IIdentityRepository identityRepository)
        {
            _tokenOperations = tokenOperations;
            _identityRepository = identityRepository;
        }

        /// <summary>
        /// Controls before function of Register
        /// </summary>
        /// <param name="register"></param>
        /// <returns></returns>
        public IResponse Register(RegisterDto register)
        {
            //Empty data check
            if (string.IsNullOrEmpty(register.Name) || string.IsNullOrEmpty(register.Lastname) || string.IsNullOrEmpty(register.PasswordConfirm) || 
                string.IsNullOrEmpty(register.Password) || string.IsNullOrEmpty(register.Email) || string.IsNullOrEmpty(register.Mobile))
                return new Response(false, Constants.emptyInput);

            //Password confirm check
            if(register.Password != register.PasswordConfirm)
                return new Response(false, Constants.passwordConfirmError);

            //Mobile Used Before Check
            if (_identityRepository.IsMobileUsed(register.Mobile))
                return new Response(false, Constants.mobileUsedError);

            //Email Used Before Check
            if (_identityRepository.IsEmailUsed(register.Email))
                return new Response(false, Constants.emailUsedError);

            register.Password = CoreHelpers.MD5Hash(register.Password); //Password MD5 Hashing

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of Login
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public IResponse Login(LoginDto login)
        {
            //Empty data check
            if (string.IsNullOrEmpty(login.Email) || string.IsNullOrEmpty(login.Password))
                return new Response(false, Constants.emptyInput);

            var isEmailCheck = login.Email.IndexOf("@"); //"@" is used in SQL Injection control, but it has to be in the e-mail, it is checked to distinguish it.

            //SQL Injection Checker
            if ((CoreHelpers.SqlInjectionChecker(login.Email) && isEmailCheck == -1) || CoreHelpers.SqlInjectionChecker(login.Password))
                return new Response(false, Constants.inputForbiddenWordError);

            return new Response(true);
        }

    }
}
