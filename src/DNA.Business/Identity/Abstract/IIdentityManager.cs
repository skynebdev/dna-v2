﻿using DNA.Domain.Dtos.Identity;
using DNA.Result.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Business.Identity.Abstract
{
    public interface IIdentityManager
    {
        IResponse Register(RegisterDto register);
      
        IResponse Login(LoginDto login);
    }
}
