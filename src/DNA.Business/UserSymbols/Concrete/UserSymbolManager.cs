﻿using DNA.Business.UserSymbols.Abstract;
using DNA.Core.Config;
using DNA.Domain.Dtos.UserSymbols;
using DNA.Repository.Identity.Authentication.Abstract;
using DNA.Result.Abstract;
using DNA.Result.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Business.UserSymbols.Concrete
{
    public class UserSymbolManager : IUserSymbolManager
    {
        private readonly ITokenOperations _tokenOperations;

        public UserSymbolManager(ITokenOperations tokenOperations)
        {
            _tokenOperations = tokenOperations;
        }

        /// <summary>
        /// Controls before function of create user symbol
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        public IResponse Create(CreateUserSymbolDto symbol)
        {
            //Empty data check
            if (string.IsNullOrEmpty(symbol.SymbolId.ToString()) || string.IsNullOrEmpty(symbol.UserId.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(symbol.Token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(symbol.UserId, symbol.Token, true))
                return new Response(false, Constants.unauthorizedError);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of update symbol
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        public IResponse Update(UpdateUserSymbolDto symbol)
        {
            //Empty data check
            if (string.IsNullOrEmpty(symbol.SymbolId.ToString()) || string.IsNullOrEmpty(symbol.UserId.ToString()) || string.IsNullOrEmpty(symbol.Id.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(symbol.Token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(symbol.UserId, symbol.Token, true))
                return new Response(false, Constants.unauthorizedError);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of GetUserSymbols
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public IResponse GetUserSymbols(Guid userId, string token)
        {
            //Empty data check
            if (string.IsNullOrEmpty(userId.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(userId, token, true))
                return new Response(false, Constants.unauthorizedError);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of delete user symbol
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        public IResponse Delete(DeleteUserSymbolDto symbol)
        {
            //Empty data check
            if (string.IsNullOrEmpty(symbol.UserId.ToString()) || string.IsNullOrEmpty(symbol.Id.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(symbol.Token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(symbol.UserId, symbol.Token, true))
                return new Response(false, Constants.unauthorizedError);

            return new Response(true);
        }
    }
}
