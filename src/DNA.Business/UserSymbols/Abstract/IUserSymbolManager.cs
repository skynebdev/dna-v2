﻿using DNA.Domain.Dtos.UserSymbols;
using DNA.Result.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Business.UserSymbols.Abstract
{
    public interface IUserSymbolManager
    {
        IResponse Create(CreateUserSymbolDto symbol);
        IResponse Update(UpdateUserSymbolDto symbol);
        IResponse GetUserSymbols(Guid userId, string token);
        IResponse Delete(DeleteUserSymbolDto symbol);

    }
}
