﻿using DNA.Domain.Dtos.ClientProducts;
using DNA.Result.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Business.ClientProducts.Abstract
{
    public interface IClientProductsManager
    {
        IResponse Create(CreateClientProductDto clientProduct);
        IResponse SetProductToCustomer(SetProductToCustomerDto clientProduct);
        IResponse GetUsersPendingApproval(Guid adminId, string token);
        IResponse GetClientProducts(Guid userId, bool type, string token);
        IResponse GetAllClientProducts(Guid adminId, string token);
        IResponse SetFreeTrial(Guid userId, string token);

    }
}
