﻿using DNA.Business.ClientProducts.Abstract;
using DNA.Core.Config;
using DNA.Domain.Dtos.ClientProducts;
using DNA.Repository.ClientProducts.Abstract;
using DNA.Repository.Identity.Authentication.Abstract;
using DNA.Result.Abstract;
using DNA.Result.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Business.ClientProducts.Concrete
{
    public class ClientProductsManager : IClientProductsManager
    {
          
        private readonly ITokenOperations _tokenOperations;
        private readonly IClientProductsRepository _clientProductsRepository;

        public ClientProductsManager(ITokenOperations tokenOperations, IClientProductsRepository clientProductsRepository)
        {
            _tokenOperations = tokenOperations;
            _clientProductsRepository = clientProductsRepository;
        }

        /// <summary>
        /// Controls before function of Create Client Products
        /// </summary>
        /// <param name="clientProduct"></param>
        /// <returns></returns>
        public IResponse Create(CreateClientProductDto clientProduct)
        {
            //Empty data check
            if (string.IsNullOrEmpty(clientProduct.UserId.ToString()) || string.IsNullOrEmpty(clientProduct.ProductId.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(clientProduct.Token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(clientProduct.UserId, clientProduct.Token, true))
                return new Response(false, Constants.unauthorizedError);

            //Is user has a still pending product request
            if (_clientProductsRepository.IsThereAlreadyPendingRequest(clientProduct.ProductId, clientProduct.UserId))
                return new Response(false, Constants.alreadyPendingRequestError);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of Set Product To Customer
        /// </summary>
        /// <param name="clientProduct"></param>
        /// <returns></returns>
        public IResponse SetProductToCustomer(SetProductToCustomerDto clientProduct)
        {
            //Empty data check
            if (string.IsNullOrEmpty(clientProduct.UserId.ToString()) || string.IsNullOrEmpty(clientProduct.Id.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(clientProduct.Token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(clientProduct.AdminId, clientProduct.Token, false))
                return new Response(false, Constants.unauthorizedError);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of Get Users Pending Approval
        /// </summary>
        /// <param name="adminId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public IResponse GetUsersPendingApproval(Guid adminId, string token)
        {
            //Empty data check
            if (string.IsNullOrEmpty(adminId.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(adminId, token, false))
                return new Response(false, Constants.unauthorizedError);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of client product by client id
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public IResponse GetClientProducts(Guid userId, bool type, string token)
        {
            //Empty data check
            if (string.IsNullOrEmpty(userId.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(userId, token, type))
                return new Response(false, Constants.unauthorizedError);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of GetAllClientProducts
        /// </summary>
        /// <param name="adminId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public IResponse GetAllClientProducts(Guid adminId, string token)
        {
            //Empty data check
            if (string.IsNullOrEmpty(adminId.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(adminId, token, false))
                return new Response(false, Constants.unauthorizedError);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of set free trial
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="adminId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public IResponse SetFreeTrial(Guid userId, string token)
        {
            //Empty data check
            if (string.IsNullOrEmpty(userId.ToString()) )
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(userId, token, true))
                return new Response(false, Constants.unauthorizedError);

            //Checking for free trial is used before
            if (_clientProductsRepository.IsFreeTrialUsed(userId))
                return new Response(false, Constants.freeTrialUsedBefore);

            return new Response(true);
        }
    }
}
