﻿using DNA.Business.Comments.Abstract;
using DNA.Core.Config;
using DNA.Domain.Dtos.Comments;
using DNA.Repository.ClientProducts.Abstract;
using DNA.Repository.Comments.Abstract;
using DNA.Repository.Identity.Authentication.Abstract;
using DNA.Result.Abstract;
using DNA.Result.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Business.Comments.Concrete
{
    public class CommentManager : ICommentManager
    {
        private readonly ITokenOperations _tokenOperations;
        private readonly IClientProductsRepository _clientProductRepository;
        private readonly ICommentRepository _commentRepository;

        public CommentManager(ITokenOperations tokenOperations, IClientProductsRepository clientProductRepository, ICommentRepository commentRepository)
        {
            _tokenOperations = tokenOperations;
            _clientProductRepository = clientProductRepository;
            _commentRepository = commentRepository;
        }

        /// <summary>
        /// Controls before function of comment create
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public IResponse Create(CreateCommentDto comment)
        {
            //Empty data check
            if (string.IsNullOrEmpty(comment.Content) || string.IsNullOrEmpty(comment.UserId.ToString()) || string.IsNullOrEmpty(comment.Title) ||
                string.IsNullOrEmpty(comment.Image) || string.IsNullOrEmpty(comment.PostId.ToString()) || string.IsNullOrEmpty(comment.SymbolId.ToString()) )
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(comment.Token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(comment.UserId, comment.Token, false))
                return new Response(false, Constants.unauthorizedError);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of comment delete
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public IResponse Delete(DeleteCommentDto comment)
        {
            //Empty data check
            if (string.IsNullOrEmpty(comment.Id.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(comment.Token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(comment.UserId, comment.Token, false))
                return new Response(false, Constants.unauthorizedError);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of update comment
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public IResponse Update(UpdateCommentDto comment)
        {
            //Empty data check
            if (string.IsNullOrEmpty(comment.Content) || string.IsNullOrEmpty(comment.UserId.ToString()) || string.IsNullOrEmpty(comment.Title) ||
                string.IsNullOrEmpty(comment.Image) || string.IsNullOrEmpty(comment.PostId.ToString()) || string.IsNullOrEmpty(comment.SymbolId.ToString()) ||
                string.IsNullOrEmpty(comment.Id.ToString()) )
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(comment.Token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(comment.UserId, comment.Token, false))
                return new Response(false, Constants.unauthorizedError);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of all comments
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public IResponse AllComments(Guid userId, bool type = true, string token = null)
        {
            //Empty data check
            if (string.IsNullOrEmpty(userId.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(userId, token, type))
                return new Response(false, Constants.unauthorizedError);

            //Checking is product buyed or not
            if (!_clientProductRepository.IsProductBuyed(userId, false)  && type )
                return new Response(false, Constants.productNotBuyed);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of comment by id
        /// </summary>
        /// <param name="commentId"></param>
        /// <param name="userId"></param>
        /// <param name="token"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public IResponse CommentById(Guid commentId, Guid userId, string token = null, bool type = true)
        {
            //Empty data check
            if (string.IsNullOrEmpty(userId.ToString()) || string.IsNullOrEmpty(commentId.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(userId, token, type))
                return new Response(false, Constants.unauthorizedError);

            //Checking is product buyed or not
            if (!_clientProductRepository.IsProductBuyed(userId, false)  && type )
                return new Response(false, Constants.productNotBuyed);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of set point
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public IResponse SetPoint(SetPointDto point)
        {
            //Empty data check
            if (string.IsNullOrEmpty(point.UserId.ToString()) || string.IsNullOrEmpty(point.Point.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(point.Token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(point.UserId, point.Token, true))
                return new Response(false, Constants.unauthorizedError);

            //Checking is product buyed or not
            if (!_clientProductRepository.IsProductBuyed(point.UserId, false)  && false)
                return new Response(false, Constants.productNotBuyed);

            if(_commentRepository.IsPointedBefore(point.UserId, point.CommentId))
                return new Response(false, Constants.allreadyPointedError);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of pin comment
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public IResponse PinComment(PinCommentDto comment)
        {
            //Empty data check
            if (string.IsNullOrEmpty(comment.UserId.ToString()) || string.IsNullOrEmpty(comment.CommentId.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(comment.Token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(comment.UserId, comment.Token, true))
                return new Response(false, Constants.unauthorizedError);

            //Checking is product buyed or not
            if (!_clientProductRepository.IsProductBuyed(comment.UserId, false)  && false)
                return new Response(false, Constants.productNotBuyed);

            if (_commentRepository.IsPinnedBefore(comment.UserId, comment.CommentId))
                return new Response(false, Constants.allreadyPinnedError);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of pin comment
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public IResponse UnpinComment(PinCommentDto comment)
        {
            //Empty data check
            if (string.IsNullOrEmpty(comment.UserId.ToString()) || string.IsNullOrEmpty(comment.CommentId.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(comment.Token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(comment.UserId, comment.Token, true))
                return new Response(false, Constants.unauthorizedError);

            //Checking is product buyed or not
            if (!_clientProductRepository.IsProductBuyed(comment.UserId, false)  && false)
                return new Response(false, Constants.productNotBuyed);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of pinned comments
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public IResponse PinnedComments(Guid userId, string token)
        {
            //Empty data check
            if (string.IsNullOrEmpty(userId.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(userId, token, true))
                return new Response(false, Constants.unauthorizedError);

            //Checking is product buyed or not
            if (!_clientProductRepository.IsProductBuyed(userId, false))
                return new Response(false, Constants.productNotBuyed);

            return new Response(true);
        }
    }
}
