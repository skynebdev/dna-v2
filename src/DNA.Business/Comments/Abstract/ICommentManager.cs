﻿using DNA.Domain.Dtos.Comments;
using DNA.Result.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Business.Comments.Abstract
{
    public interface ICommentManager
    {
        IResponse Create(CreateCommentDto comment);
        IResponse Update(UpdateCommentDto comment);
        IResponse Delete(DeleteCommentDto comment);
        IResponse AllComments(Guid userId, bool type = true, string token = null);
        IResponse CommentById(Guid commentId, Guid userId, string token = null, bool type = true);
        IResponse SetPoint(SetPointDto point);
        IResponse PinComment(PinCommentDto comment);
        IResponse UnpinComment(PinCommentDto comment);
        IResponse PinnedComments(Guid userId, string token);

    }
}
