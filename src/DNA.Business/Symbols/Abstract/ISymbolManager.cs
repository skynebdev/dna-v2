﻿using DNA.Domain.Dtos.Symbols;
using DNA.Result.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Business.Symbols.Abstract
{
    public interface ISymbolManager
    {
        IResponse Create(CreateSymbolDto symbol);
        IResponse Update(UpdateSymbolDto symbol);
        IResponse Delete(DeleteSymbolDto symbol);
    }
}
