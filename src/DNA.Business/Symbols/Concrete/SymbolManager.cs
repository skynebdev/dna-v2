﻿using DNA.Business.Symbols.Abstract;
using DNA.Core.Config;
using DNA.Domain.Dtos.Symbols;
using DNA.Repository.Identity.Authentication.Abstract;
using DNA.Result.Abstract;
using DNA.Result.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Business.Symbols.Concrete
{
    public class SymbolManager : ISymbolManager
    {

        private readonly ITokenOperations _tokenOperations;

        public SymbolManager(ITokenOperations tokenOperations)
        {
            _tokenOperations = tokenOperations;
        }

        /// <summary>
        /// Controls before function of Create Symbol
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        public IResponse Create(CreateSymbolDto symbol)
        {
            //Empty data check
            if (string.IsNullOrEmpty(symbol.Name) || string.IsNullOrEmpty(symbol.Source) || string.IsNullOrEmpty(symbol.Description) )
                return new Response(false, Constants.emptyInput);
            
            //Token Check
            if(string.IsNullOrEmpty(symbol.Token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if(!_tokenOperations.IsTrueToken(symbol.UserId, symbol.Token, false))
                return new Response(false, Constants.unauthorizedError);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of Update Symbol
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        public IResponse Update(UpdateSymbolDto symbol)
        {
            //Empty data check
            if (string.IsNullOrEmpty(symbol.Name) || string.IsNullOrEmpty(symbol.Source) || string.IsNullOrEmpty(symbol.Description) || string.IsNullOrEmpty(symbol.Id.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(symbol.Token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(symbol.UserId, symbol.Token, false))
                return new Response(false, Constants.unauthorizedError);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of Delete Symbol
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public IResponse Delete(DeleteSymbolDto symbol)
        {
            //Empty data check
            if (string.IsNullOrEmpty(symbol.Id.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(symbol.Token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(symbol.UserId, symbol.Token, false))
                return new Response(false, Constants.unauthorizedError);

            return new Response(true);
        }
    }
}
