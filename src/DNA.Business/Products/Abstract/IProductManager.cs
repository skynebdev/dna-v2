﻿using DNA.Domain.Dtos.Products;
using DNA.Result.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Business.Products.Abstract
{
    public interface IProductManager
    {
        IResponse Create(CreateProductDto comment);
        IResponse Update(UpdateProductDto comment);
        IResponse Delete(DeleteProductDto comment);
        IResponse AllProducts(Guid userId, bool type = true, string token = null);
        IResponse ProductById(Guid productId, Guid userId, string token = null, bool type = true);
    }
}
