﻿using DNA.Business.Products.Abstract;
using DNA.Core.Config;
using DNA.Domain.Dtos.Products;
using DNA.Repository.Identity.Authentication.Abstract;
using DNA.Result.Abstract;
using DNA.Result.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Business.Products.Concrete
{
    public class ProductManager : IProductManager
    {
        private readonly ITokenOperations _tokenOperations;

        public ProductManager(ITokenOperations tokenOperations)
        {
            _tokenOperations = tokenOperations;
        }

        /// <summary>
        /// Controls before function of comment create
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public IResponse Create(CreateProductDto comment)
        {
            //Empty data check
            if (string.IsNullOrEmpty(comment.Period.ToString()) || string.IsNullOrEmpty(comment.UserId.ToString()) || string.IsNullOrEmpty(comment.Name) ||
                string.IsNullOrEmpty(comment.Description) || string.IsNullOrEmpty(comment.Price.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(comment.Token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(comment.UserId, comment.Token, false))
                return new Response(false, Constants.unauthorizedError);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of comment delete
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public IResponse Delete(DeleteProductDto comment)
        {
            //Empty data check
            if (string.IsNullOrEmpty(comment.Id.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(comment.Token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(comment.UserId, comment.Token, false))
                return new Response(false, Constants.unauthorizedError);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of update comment
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public IResponse Update(UpdateProductDto comment)
        {
            //Empty data check
            if (string.IsNullOrEmpty(comment.Period.ToString()) || string.IsNullOrEmpty(comment.UserId.ToString()) || string.IsNullOrEmpty(comment.Name) ||
                string.IsNullOrEmpty(comment.Description) || string.IsNullOrEmpty(comment.Price.ToString()) || string.IsNullOrEmpty(comment.Id.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(comment.Token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(comment.UserId, comment.Token, false))
                return new Response(false, Constants.unauthorizedError);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of all comments
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public IResponse AllProducts(Guid userId, bool type = true, string token = null)
        {
            //Empty data check
            if (string.IsNullOrEmpty(userId.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(userId, token, type))
                return new Response(false, Constants.unauthorizedError);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of comment by id
        /// </summary>
        /// <param name="commentId"></param>
        /// <param name="userId"></param>
        /// <param name="token"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public IResponse ProductById(Guid productId, Guid userId, string token = null, bool type = true)
        {
            //Empty data check
            if (string.IsNullOrEmpty(userId.ToString()) || string.IsNullOrEmpty(productId.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(userId, token, type))
                return new Response(false, Constants.unauthorizedError);

            return new Response(true);
        }
    }
}
