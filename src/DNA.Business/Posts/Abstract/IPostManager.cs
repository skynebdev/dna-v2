﻿using DNA.Domain.Dtos.Posts;
using DNA.Result.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Business.Posts.Abstract
{
    public interface IPostManager
    {
        IResponse Create(CreatePostDto post);
        IResponse Update(UpdatePostDto post);
        IResponse Delete(DeletePostDto post);
        IResponse AllPosts(Guid userId, bool type, string token);
        IResponse PostById(Guid postId, Guid userId, string token, bool type = true);
        IResponse PinPost(PinPostDto post);
        IResponse UnpinPost(PinPostDto post);
        IResponse PinnedPosts(Guid userId, string token);
    }
}
