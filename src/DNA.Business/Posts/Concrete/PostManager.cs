﻿using DNA.Business.Posts.Abstract;
using DNA.Core.Config;
using DNA.Domain.Dtos.Posts;
using DNA.Repository.ClientProducts.Abstract;
using DNA.Repository.Identity.Authentication.Abstract;
using DNA.Repository.Posts.Abstract;
using DNA.Result.Abstract;
using DNA.Result.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Business.Posts.Concrete
{
    public class PostManager : IPostManager
    {
        private readonly ITokenOperations _tokenOperations;
        private readonly IClientProductsRepository _clientProductRepository;
        private readonly IPostRepository _postRepository;

        public PostManager(ITokenOperations tokenOperations, IClientProductsRepository clientProductRepository, IPostRepository postRepository)
        {
            _tokenOperations = tokenOperations;
            _clientProductRepository = clientProductRepository;
            _postRepository = postRepository;
        }

        /// <summary>
        /// Controls before function of Create Post
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        public IResponse Create(CreatePostDto post)
        {
            //Empty data check
            if (string.IsNullOrEmpty(post.Source) || string.IsNullOrEmpty(post.UserId.ToString()) || string.IsNullOrEmpty(post.Content) ||
                string.IsNullOrEmpty(post.Title) || string.IsNullOrEmpty(post.SymbolId.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(post.Token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(post.UserId, post.Token, false))
                return new Response(false, Constants.unauthorizedError);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of Update Post
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public IResponse Update(UpdatePostDto post)
        {
            //Empty data check
            if (string.IsNullOrEmpty(post.Source) || string.IsNullOrEmpty(post.UserId.ToString()) || string.IsNullOrEmpty(post.Content) ||
                string.IsNullOrEmpty(post.Title) || string.IsNullOrEmpty(post.SymbolId.ToString()) || string.IsNullOrEmpty(post.Id.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(post.Token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(post.UserId, post.Token, false))
                return new Response(false, Constants.unauthorizedError);

            return new Response(true);
        }
       
        /// <summary>
        /// Controls before funciton of Delete Post
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        public IResponse Delete(DeletePostDto post)
        {
            //Empty data check
            if (string.IsNullOrEmpty(post.UserId.ToString()) || string.IsNullOrEmpty(post.Id.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(post.Token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(post.UserId, post.Token, false))
                return new Response(false, Constants.unauthorizedError);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of all posts
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public IResponse AllPosts(Guid userId, bool type, string token)
        {
            //Empty data check
            if (string.IsNullOrEmpty(userId.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(userId, token, type))
                return new Response(false, Constants.unauthorizedError);

            //Checking is product buyed or not (only for users)
            if (!_clientProductRepository.IsProductBuyed(userId,true) && type)
                return new Response(false, Constants.productNotBuyed);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of post by id
        /// </summary>
        /// <param name="postId"></param>
        /// <param name="userId"></param>
        /// <param name="token"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public IResponse PostById(Guid postId, Guid userId, string token, bool type = true)
        {
            //Empty data check
            if (string.IsNullOrEmpty(userId.ToString()) || string.IsNullOrEmpty(postId.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(userId, token, type))
                return new Response(false, Constants.unauthorizedError);

            //Checking is product buyed or not (only for users)
            if (!_clientProductRepository.IsProductBuyed(userId, true) && type)
                return new Response(false, Constants.productNotBuyed);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of pin comment
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public IResponse PinPost(PinPostDto post)
        {
            //Empty data check
            if (string.IsNullOrEmpty(post.UserId.ToString()) || string.IsNullOrEmpty(post.PostId.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(post.Token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(post.UserId, post.Token, true))
                return new Response(false, Constants.unauthorizedError);

            //Checking is product buyed or not
            if (!_clientProductRepository.IsProductBuyed(post.UserId, true))
                return new Response(false, Constants.productNotBuyed);

            if (_postRepository.IsPinnedBefore(post.UserId, post.PostId))
                return new Response(false, Constants.allreadyPinnedError);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of pin posts
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public IResponse UnpinPost(PinPostDto post)
        {
            //Empty data check
            if (string.IsNullOrEmpty(post.UserId.ToString()) || string.IsNullOrEmpty(post.PostId.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(post.Token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(post.UserId, post.Token, true))
                return new Response(false, Constants.unauthorizedError);

            //Checking is product buyed or not
            if (!_clientProductRepository.IsProductBuyed(post.UserId, true))
                return new Response(false, Constants.productNotBuyed);

            return new Response(true);
        }

        /// <summary>
        /// Controls before function of pinned posts
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public IResponse PinnedPosts(Guid userId, string token)
        {
            //Empty data check
            if (string.IsNullOrEmpty(userId.ToString()))
                return new Response(false, Constants.emptyInput);

            //Token Check
            if (string.IsNullOrEmpty(token))
                return new Response(false, Constants.unauthorizedError);

            //True Token Check
            if (!_tokenOperations.IsTrueToken(userId, token, true))
                return new Response(false, Constants.unauthorizedError);

            //Checking is product buyed or not
            if (!_clientProductRepository.IsProductBuyed(userId, false))
                return new Response(false, Constants.productNotBuyed);

            return new Response(true);
        }
    }
}
