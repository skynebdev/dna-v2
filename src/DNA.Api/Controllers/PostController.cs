﻿using DNA.Application.Posts.Abstract;
using DNA.Domain.Dtos.Posts;
using DNA.Result.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DNA.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController][Authorize]
    public class PostController : ControllerBase
    {
        private readonly IPostService _postService;

        public PostController(IPostService postService)
        {
            _postService = postService;
        }

        /// <summary>
        /// Create a new post
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IResponse> Create([FromBody] CreatePostDto post)
        {
            post.Token = Request.Headers["Authorization"];
            return await _postService.Create(post);
        }

        /// <summary>
        /// Update post by id
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IResponse> Update([FromBody] UpdatePostDto post)
        {
            post.Token = Request.Headers["Authorization"];
            return await _postService.Update(post);
        }

        /// <summary>
        /// Delete post by id
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IResponse> Delete([FromBody] DeletePostDto post)
        {
            post.Token = Request.Headers["Authorization"];
            return await _postService.Delete(post);
        }

        /// <summary>
        /// Get All Posts
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpGet]
        public IResponse AllPosts(Guid userId, bool type = true)
        {
            var token = Request.Headers["Authorization"];
            return _postService.AllPosts(userId, type, token);
        }

        /// <summary>
        /// Getting a post by id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IResponse> PostById(Guid postId, Guid userId, bool type=true)
        {
            var token = Request.Headers["Authorization"];
            return await _postService.PostById(postId, userId, token, type);
        }

        /// <summary>
        /// Pinned a Post
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IResponse> PinPost([FromBody] PinPostDto post)
        {
            post.Token = Request.Headers["PinPost"];
            return await _postService.PinPost(post);
        }

        /// <summary>
        /// Unpinned a post
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IResponse> UnpinPost([FromBody] PinPostDto post)
        {
            post.Token = Request.Headers["Authorization"];
            return await _postService.UnpinPost(post);
        }

        /// <summary>
        /// Returned pinned posts of user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        public IResponse PinnedPosts(Guid userId)
        {
            var token = Request.Headers["Authorization"];
            return _postService.PinnedPosts(userId, token);
        }
    }
}
