﻿using DNA.Application.UserSymbols.Abstract;
using DNA.Domain.Dtos.UserSymbols;
using DNA.Result.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DNA.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController][Authorize]
    public class UserSymbolsController : ControllerBase
    {
        private readonly IUserSymbolService _userSymbolService;

        public UserSymbolsController(IUserSymbolService userSymbolService)
        {
            _userSymbolService = userSymbolService;
        }

        /// <summary>
        /// Create new symbol for client
        /// </summary>
        /// <param name="clientProduct"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IResponse> Create([FromBody] CreateUserSymbolDto symbol)
        {
            symbol.Token = Request.Headers["Authorization"];
            return await _userSymbolService.Create(symbol);
        }

        /// <summary>
        /// Update symbol for client
        /// </summary>
        /// <param name="clientProduct"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IResponse> Update([FromBody] UpdateUserSymbolDto symbol)
        {
            symbol.Token = Request.Headers["Authorization"];
            return await _userSymbolService.Update(symbol);
        }

        /// <summary>
        /// Delete symbol for client
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IResponse> Delete([FromBody] DeleteUserSymbolDto symbol)
        {
            symbol.Token = Request.Headers["Authorization"];
            return await _userSymbolService.Delete(symbol);
        }

        /// <summary>
        /// Get symbols for client
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        public IResponse GetUserSymbols(Guid userId)
        {
            var token = Request.Headers["Authorization"];
            return _userSymbolService.GetUserSymbols(userId, token);
        }
    }
}
