﻿using DNA.Application.Identity.Abstract;
using DNA.Domain.Dtos.Identity;
using DNA.Result.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DNA.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController][Authorize]
    public class IdentityController : ControllerBase
    {
        private readonly IIdentityService _identityService;

        public IdentityController(IIdentityService identityService)
        {
            _identityService = identityService;
        }

        /// <summary>
        /// Registering new user
        /// </summary>
        /// <param name="register"></param>
        /// <returns></returns>
        [HttpPost][AllowAnonymous]
        public async Task<IResponse> Register([FromBody] RegisterDto register)
        {
            return await _identityService.Register(register);
        }

        /// <summary>
        /// User or Admin Login
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost][AllowAnonymous]
        public async Task<IResponse> Login([FromBody] LoginDto login)
        {
            return await _identityService.Login(login);
        }
    }
}