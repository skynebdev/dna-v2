﻿using DNA.Application.ClientProducts.Abstract;
using DNA.Domain.Dtos.ClientProducts;
using DNA.Result.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DNA.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController][Authorize]
    public class ClientProductController : ControllerBase
    {
        private readonly IClientProductService _clientProductService;

        public ClientProductController(IClientProductService clientProductService)
        {
            _clientProductService = clientProductService;
        }

        /// <summary>
        /// Create new product for client
        /// </summary>
        /// <param name="clientProduct"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IResponse> Create([FromBody] CreateClientProductDto clientProduct)
        {
            clientProduct.Token = Request.Headers["Authorization"];
            return await _clientProductService.Create(clientProduct);
        }

        /// <summary>
        /// Set a waiting product to customer
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IResponse> SetProductToCustomer([FromBody] SetProductToCustomerDto product)
        {
            product.Token = Request.Headers["Authorization"];
            return await _clientProductService.SetProductToCustomer(product);
        }

        /// <summary>
        /// Set a waiting product to customer
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpGet]
        public IResponse GetUsersPendingApproval(Guid adminId)
        {
            var token = Request.Headers["Authorization"];
            return _clientProductService.GetUsersPendingApproval(adminId,token);
        }

        /// <summary>
        /// Client products by client ıd
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpGet]
        public IResponse GetClientProducts(Guid userId, bool type = true)
        {
            var token = Request.Headers["Authorization"];
            return _clientProductService.GetClientProducts(userId, type, token);
        }

        /// <summary>
        /// Client products by client ıd
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpGet]
        public IResponse GetAllClientProducts(Guid adminId)
        {
            var token = Request.Headers["Authorization"];
            return _clientProductService.GetAllClientProducts(adminId, token);
        }
        
        /// <summary>
        /// Set Free Trial to user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IResponse> SetFreeTrial(Guid userId)
        {
            var token = Request.Headers["Authorization"];
            return await _clientProductService.SetFreeTrial(userId, token);
        }
    }
}
