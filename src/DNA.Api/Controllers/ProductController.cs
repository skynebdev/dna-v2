﻿using DNA.Application.Products.Abstract;
using DNA.Domain.Dtos.Products;
using DNA.Result.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DNA.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController][Authorize]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        /// <summary>
        /// Create a new post
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IResponse> Create([FromBody] CreateProductDto post)
        {
            post.Token = Request.Headers["Authorization"];
            return await _productService.Create(post);
        }

        /// <summary>
        /// Update post by id
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IResponse> Update([FromBody] UpdateProductDto post)
        {
            post.Token = Request.Headers["Authorization"];
            return await _productService.Update(post);
        }

        /// <summary>
        /// Delete post by id
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IResponse> Delete([FromBody] DeleteProductDto post)
        {
            post.Token = Request.Headers["Authorization"];
            return await _productService.Delete(post);
        }

        /// <summary>
        /// Get All Posts
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpGet]
        public IResponse AllProducts(Guid userId, bool type = true)
        {
            var token = Request.Headers["Authorization"];
            return _productService.AllProducts(userId, type, token);
        }

        /// <summary>
        /// Getting a post by id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IResponse ProductById(Guid productId, Guid userId, bool type = true)
        {
            var token = Request.Headers["Authorization"];
            return _productService.ProductById(productId, userId, token, type);
        }
    }
}
