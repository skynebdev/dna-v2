﻿using DNA.Application.Comments.Abstract;
using DNA.Domain.Dtos.Comments;
using DNA.Result.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DNA.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController][Authorize]
    public class CommentController : ControllerBase
    {
        private readonly ICommentService _commentService;
        
        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        /// <summary>
        /// Create a new post
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IResponse> Create([FromBody] CreateCommentDto post)
        {
            post.Token = Request.Headers["Authorization"];
            return await _commentService.Create(post);
        }

        /// <summary>
        /// Update post by id
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IResponse> Update([FromBody] UpdateCommentDto post)
        {
            post.Token = Request.Headers["Authorization"];
            return await _commentService.Update(post);
        }

        /// <summary>
        /// Delete post by id
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IResponse> Delete([FromBody] DeleteCommentDto post)
        {
            post.Token = Request.Headers["Authorization"];
            return await _commentService.Delete(post);
        }

        /// <summary>
        /// Get All Posts
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpGet]
        public IResponse AllComments(Guid userId, bool type = true)
        {
            var token = Request.Headers["Authorization"];
            return _commentService.AllComments(userId, type, token);
        }
        
        /// <summary>
        /// Getting a post by id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IResponse CommentById(Guid commentId, Guid userId, bool type = true)
        {
            var token = Request.Headers["Authorization"];
            return _commentService.CommentById(commentId, userId, token, type);
        }

        /// <summary>
        /// Set point to comment
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IResponse> SetPoint([FromBody] SetPointDto point)
        {
            point.Token = Request.Headers["Authorization"];
            return await _commentService.SetPoint(point);
        }

        /// <summary>
        /// Pinned a comment
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IResponse> PinComment([FromBody] PinCommentDto comment)
        {
            comment.Token = Request.Headers["Authorization"];
            return await _commentService.PinComment(comment);
        }

        /// <summary>
        /// Pinned a comment
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IResponse> UnpinComment([FromBody] PinCommentDto comment)
        {
            comment.Token = Request.Headers["Authorization"];
            return await _commentService.UnpinComment(comment);
        }

        /// <summary>
        /// Returned pinned comments of user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        public IResponse PinnedComments(Guid userId)
        {
            var token = Request.Headers["Authorization"];
            return _commentService.PinnedComments(userId, token);
        }
    }
}
