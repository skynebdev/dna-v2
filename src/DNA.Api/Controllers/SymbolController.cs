﻿using DNA.Application.Symbols.Abstract;
using DNA.Domain.Dtos.Symbols;
using DNA.Result.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DNA.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController][Authorize]
    public class SymbolController : ControllerBase
    {
        private readonly ISymbolService _symbolService;

        public SymbolController(ISymbolService symbolService)
        {
            _symbolService = symbolService;
        }

        /// <summary>
        /// Create a new symbol
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IResponse> Create([FromBody] CreateSymbolDto symbol)
        {
            symbol.Token = Request.Headers["Authorization"];
            return await _symbolService.Create(symbol);
        }

        /// <summary>
        /// Update a current symbol
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IResponse> Update([FromBody] UpdateSymbolDto symbol)
        {
            symbol.Token = Request.Headers["Authorization"];
            return await _symbolService.Update(symbol);
        }

        /// <summary>
        /// Update a current symbol
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IResponse> Delete([FromBody] DeleteSymbolDto symbol)
        {
            symbol.Token = Request.Headers["Authorization"];
            return await _symbolService.Delete(symbol);
        }

        /// <summary>
        /// Returned all symbols
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        [HttpGet][AllowAnonymous]
        public IResponse AllSymbols()
        {
            return _symbolService.AllSymbols();
        }
    }
}
