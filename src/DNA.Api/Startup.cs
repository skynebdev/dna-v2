using DNA.Application.ClientProducts.Abstract;
using DNA.Application.ClientProducts.Concrete;
using DNA.Application.Comments.Abstract;
using DNA.Application.Comments.Concrete;
using DNA.Application.Identity.Abstract;
using DNA.Application.Identity.Concrete;
using DNA.Application.Posts.Abstract;
using DNA.Application.Posts.Concrete;
using DNA.Application.Products.Abstract;
using DNA.Application.Products.Concrete;
using DNA.Application.Symbols.Abstract;
using DNA.Application.Symbols.Concrete;
using DNA.Application.UserSymbols.Abstract;
using DNA.Application.UserSymbols.Concrete;
using DNA.Business.ClientProducts.Abstract;
using DNA.Business.ClientProducts.Concrete;
using DNA.Business.Comments.Abstract;
using DNA.Business.Comments.Concrete;
using DNA.Business.Identity.Abstract;
using DNA.Business.Identity.Concrete;
using DNA.Business.Posts.Abstract;
using DNA.Business.Posts.Concrete;
using DNA.Business.Products.Abstract;
using DNA.Business.Products.Concrete;
using DNA.Business.Symbols.Abstract;
using DNA.Business.Symbols.Concrete;
using DNA.Business.UserSymbols.Abstract;
using DNA.Business.UserSymbols.Concrete;
using DNA.Core.AutoMapper;
using DNA.EntityFrameworkCore.Context;
using DNA.Repository.ClientProducts.Abstract;
using DNA.Repository.ClientProducts.Concrete;
using DNA.Repository.Comments.Abstract;
using DNA.Repository.Comments.Concrete;
using DNA.Repository.Identity.Abstract;
using DNA.Repository.Identity.Authentication.Abstract;
using DNA.Repository.Identity.Authentication.Concrete;
using DNA.Repository.Identity.Concrete;
using DNA.Repository.Posts.Abstract;
using DNA.Repository.Posts.Concrete;
using DNA.Repository.Products.Abstract;
using DNA.Repository.Products.Concrete;
using DNA.Repository.Symbols.Abstract;
using DNA.Repository.Symbols.Concrete;
using DNA.Repository.UserSymbols.Abstract;
using DNA.Repository.UserSymbols.Concrete;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region JWT Authentication
            services.AddAuthentication(opt =>
            {
                opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Configuration["Jwt:Issuer"],
                    ValidAudience = Configuration["Jwt:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:SecurityKey"]))
                };
            });
            #endregion

            #region EF Core
            string mySqlConnectionStr = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContextPool<DNADbContext>(options => options.UseMySql(mySqlConnectionStr, ServerVersion.AutoDetect(mySqlConnectionStr)));
            #endregion

            #region Auto Mapper
            services.AddAutoMapper(typeof(MapProfiles));
            #endregion

            #region Dependency Injection
            services.AddScoped<IIdentityService, IdentityService>();
            services.AddScoped<IIdentityRepository, IdentityRepository>();
            services.AddScoped<ITokenOperations, TokenOperations>();
            services.AddScoped<IIdentityManager, IdentityManager>();
            
            services.AddScoped<ISymbolService, SymbolService>();
            services.AddScoped<ISymbolRepository, SymbolRepository>();
            services.AddScoped<ISymbolManager, SymbolManager>();

            services.AddScoped<IPostService, PostService>();
            services.AddScoped<IPostRepository, PostRepository>();
            services.AddScoped<IPostManager, PostManager>();

            services.AddScoped<ICommentService, CommentService>();
            services.AddScoped<ICommentRepository, CommentRepository>();
            services.AddScoped<ICommentManager, CommentManager>();

            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<IProductManager, ProductManager>();

            services.AddScoped<IClientProductService, ClientProductService>();
            services.AddScoped<IClientProductsRepository, ClientProductsRepository>();
            services.AddScoped<IClientProductsManager, ClientProductsManager>();
            
            services.AddScoped<IUserSymbolService, UserSymbolService>();
            services.AddScoped<IUserSymbolManager, UserSymbolManager>();
            services.AddScoped<IUserSymbolRepository, UserSymbolRepository>();
            #endregion

            #region Solution Configurations
            services.AddControllers();
            #endregion
            
            #region Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "DNAApi", Version = "v1" });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,
                        },
                        new List<string>()
                    }
                });
            });
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "DNA.Api v1"));
            }

          //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseCors(x => x
              .AllowAnyMethod()
              .AllowAnyHeader()
              .SetIsOriginAllowed(origin => true)
              .AllowCredentials());

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
