﻿using DNA.Domain.SeedWorks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Domain.Dtos.Comments
{
    public class DeleteCommentDto : GlobalAccess
    {
        public Guid Id { get; set; }

    }
}
