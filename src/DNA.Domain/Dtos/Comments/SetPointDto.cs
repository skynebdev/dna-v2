﻿using DNA.Domain.SeedWorks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Domain.Dtos.Comments
{
    public class SetPointDto : GlobalAccess
    {
        public Guid CommentId { get; set; }
        public int Point { get; set; }
    }
}
