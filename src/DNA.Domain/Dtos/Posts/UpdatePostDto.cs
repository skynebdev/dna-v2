﻿using DNA.Domain.Enums.Posts;
using DNA.Domain.SeedWorks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Domain.Dtos.Posts
{
    public class UpdatePostDto : GlobalAccess
    {
        public Guid Id { get; set; }    
        public string Title { get; set; }
        public string Content { get; set; }
        public string Source { get; set; }
        public PostStatuses Status { get; set; } = PostStatuses.Active;
        public Guid SymbolId { get; set; }
    }
}
