﻿using DNA.Domain.SeedWorks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Domain.Dtos.Posts
{
    public class PinPostDto : GlobalAccess
    {
        public Guid PostId { get; set; }
    }
}
