﻿using DNA.Domain.Enums.Products;
using DNA.Domain.SeedWorks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Domain.Dtos.Products
{
    public class CreateProductDto : GlobalAccess
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public int Period { get; set; }
        public ProductTypes Type { get; set; }
    }
}
