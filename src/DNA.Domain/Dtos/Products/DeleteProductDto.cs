﻿using DNA.Domain.SeedWorks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Domain.Dtos.Products
{
    public class DeleteProductDto : GlobalAccess
    {
        public Guid Id { get; set; }
    }
}
