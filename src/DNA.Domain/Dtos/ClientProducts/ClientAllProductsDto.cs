﻿using DNA.Domain.Enums.ClientProducts;
using DNA.Domain.Enums.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Domain.Dtos.ClientProducts
{
    public class ClientAllProductsDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public double ProductPrice { get; set; }
        public int ProductPeriod { get; set; }
        public ProductTypes ProductType { get; set; }
        public ProductStatuses Status { get; set; }
    }
}
