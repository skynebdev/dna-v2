﻿using DNA.Domain.Enums.ClientProducts;
using DNA.Domain.SeedWorks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Domain.Dtos.ClientProducts
{
    public class SetProductToCustomerDto : GlobalAccess
    {
        public Guid Id { get; set; }
        public Guid AdminId { get; set; }
        public Guid ProductId { get; set; }

        public DateTime ExpiredDate;

        public ProductStatuses Status = ProductStatuses.Active;
    }
}
