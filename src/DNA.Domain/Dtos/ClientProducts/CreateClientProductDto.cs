﻿using DNA.Domain.Enums.ClientProducts;
using DNA.Domain.SeedWorks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Domain.Dtos.ClientProducts
{
    public class CreateClientProductDto : GlobalAccess
    {
        public Guid ProductId { get; set; }

        public ProductStatuses Status = ProductStatuses.Pending;

        public DateTime ExpiredDate = DateTime.Now;
    }
}
