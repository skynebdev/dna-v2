﻿using DNA.Domain.Enums.ClientProducts;
using DNA.Domain.Enums.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Domain.Dtos.ClientProducts
{
    public class ClientProductByClientDto
    {
        public List<ProductList> Products { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
    }

    public class ProductList
    {
        public Guid Id { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public int Period { get; set; }
        public ProductTypes Type { get; set; }
        public ProductStatuses Status { get; set; }

    }
}
