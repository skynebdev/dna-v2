﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Domain.Dtos.UserSymbols
{
    public class GetUserSymbolsDto
    {
        public Guid Id { get; set; }
        public Guid SymbolId { get; set; }
        public string SymbolName { get; set; }
        public string SymbolDescription { get; set; }
        public string SymbolSource { get; set; }
    }
}
