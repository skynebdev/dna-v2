﻿using DNA.Domain.SeedWorks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Domain.Dtos.UserSymbols
{
    public class UpdateUserSymbolDto : GlobalAccess
    {
        public Guid Id { get; set; }
        public Guid SymbolId { get; set; }
    }
}
