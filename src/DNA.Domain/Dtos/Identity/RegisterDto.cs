﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Domain.Dtos.Identity
{
    public class RegisterDto
    {
        [Required][EmailAddress]
        public string Email { get; set; }

        [Required][MaxLength(100)][MinLength(2)]
        public string Name { get; set; }

        [Required][MaxLength(100)][MinLength(2)]
        public string Lastname { get; set; }

        [Required][MinLength(8)]
        public string Mobile { get; set; }

        [Required][MinLength(8)]
        public string Password { get; set; }

        [Required][MinLength(8)]
        public string PasswordConfirm { get; set; }
    }
}
