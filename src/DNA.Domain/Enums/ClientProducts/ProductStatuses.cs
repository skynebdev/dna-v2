﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Domain.Enums.ClientProducts
{
    public enum ProductStatuses
    {
        Pending,
        Active,
        Deactive
    }
}
