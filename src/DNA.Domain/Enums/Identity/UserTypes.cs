﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Domain.Enums.Identity
{
    public enum UserTypes
    {
        User,
        Admin
    }
}
