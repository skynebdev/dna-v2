﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Domain.SeedWorks
{
    public class GlobalAccess
    {
        public Guid UserId { get; set; }

        public string Token;
    }
}
