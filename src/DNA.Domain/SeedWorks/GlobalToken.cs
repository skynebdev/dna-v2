﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Domain.SeedWorks
{
    public class GlobalToken
    {
        public string Token { get; set; }
        public DateTime TokenExpireDate { get; set; }
    }
}
