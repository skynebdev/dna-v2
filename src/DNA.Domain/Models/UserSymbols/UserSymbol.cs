﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Domain.Models.UserSymbols
{
    public class UserSymbol
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid SymbolId { get; set; }
    }
}
