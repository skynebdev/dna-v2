﻿using DNA.Domain.SeedWorks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Domain.Models
{
    public class User : GlobalToken
    {
        public Guid Id { get; set; }
        public string Name { get; set; }    
        public string Lastname { get; set; }    
        public string Email { get; set; }    
        public string Mobile { get; set; }    
        public string Password { get; set; }    

    }
}
