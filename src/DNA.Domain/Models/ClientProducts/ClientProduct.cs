﻿using DNA.Domain.Enums.ClientProducts;
using DNA.Domain.Enums.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Domain.Models.ClientProducts
{
    public class ClientProduct
    {
        public Guid Id { get; set; }
        public DateTime ExpiredDate { get; set; }
        public Guid ProductId { get; set; }
        public Guid UserId { get; set; }
        public ProductStatuses Status { get; set; }
    }
}
