﻿using AutoMapper;
using DNA.Domain.Dtos.UserSymbols;
using DNA.Domain.Models.UserSymbols;
using DNA.EntityFrameworkCore.Context;
using DNA.Repository.UserSymbols.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Repository.UserSymbols.Concrete
{
    public class UserSymbolRepository : IUserSymbolRepository
    {
        private DNADbContext dbContext;
        public IMapper _mapper;

        public UserSymbolRepository(DNADbContext context, IMapper mapper)
        {
            dbContext = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Creating new symbol on database
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        public async Task<bool> Create(CreateUserSymbolDto symbol)
        {
            await dbContext.UserSymbols.AddAsync(_mapper.Map<UserSymbol>(symbol));
            var isCreated = await dbContext.SaveChangesAsync();

            return isCreated > 0;
        }

        /// <summary>
        /// Update a symbol on database
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        public async Task<bool> Update(UpdateUserSymbolDto symbol)
        {
            var currentSymbol = dbContext.UserSymbols.Where(i => i.Id == symbol.Id).FirstOrDefault();

            if (currentSymbol is null)
                return false;

            currentSymbol.SymbolId = symbol.SymbolId;
            var isUpdated = await dbContext.SaveChangesAsync();

            return isUpdated > 0;
        }

        /// <summary>
        /// Returned users symbols
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<GetUserSymbolsDto> GetUserSymbols(Guid userId)
        {

            var userSymbols = (from usersymbols in dbContext.UserSymbols
                               join symbols in dbContext.Symbols on usersymbols.SymbolId equals symbols.Id
                               where usersymbols.UserId == userId
                               select new GetUserSymbolsDto
                               {
                                   Id = usersymbols.Id,
                                   SymbolDescription = symbols.Description,
                                   SymbolId = usersymbols.SymbolId,
                                   SymbolName = symbols.Name,
                                   SymbolSource = symbols.Source
                               }).ToList();

            return userSymbols;
        }

        /// <summary>
        /// Delete user symbol from db
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        public async Task<bool> Delete(DeleteUserSymbolDto symbol)
        {
            var currentSymbol = dbContext.UserSymbols.Where(i => i.Id == symbol.Id).FirstOrDefault();
            if (currentSymbol is null)
                return false;

            dbContext.Remove(currentSymbol);
            var isDeleted = await dbContext.SaveChangesAsync();

            return isDeleted > 0;
        }
    }
}
