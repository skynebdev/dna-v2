﻿using DNA.Domain.Dtos.UserSymbols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Repository.UserSymbols.Abstract
{
    public interface IUserSymbolRepository
    {
        Task<bool> Create(CreateUserSymbolDto symbol);
        Task<bool> Update(UpdateUserSymbolDto symbol);
        List<GetUserSymbolsDto> GetUserSymbols(Guid userId);
        Task<bool> Delete(DeleteUserSymbolDto symbol);

    }
}
