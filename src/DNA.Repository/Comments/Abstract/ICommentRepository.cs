﻿using DNA.Domain.Dtos.Comments;
using DNA.Domain.Models.Comments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Repository.Comments.Abstract
{
    public interface ICommentRepository
    {
        Task<bool> Create(CreateCommentDto post);
        Task<bool> Update(UpdateCommentDto post);
        Task<bool> Delete(DeleteCommentDto post);
        List<Comment> AllComments();
        Comment CommentById(Guid commentId);
        Task<bool> SetPoint(SetPointDto point);
        bool IsPointedBefore(Guid userId, Guid commentId);
        bool IsPinnedBefore(Guid userId, Guid commentId);
        Task<bool> PinComment(PinCommentDto comment);
        Task<bool> UnpinComment(PinCommentDto comment);
        List<Comment> PinnedComments(Guid userId);

    }
}
