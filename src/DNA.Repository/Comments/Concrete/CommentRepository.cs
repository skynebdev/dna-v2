﻿using AutoMapper;
using DNA.Domain.Dtos.Comments;
using DNA.Domain.Models.Comments;
using DNA.EntityFrameworkCore.Context;
using DNA.Repository.Comments.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Repository.Comments.Concrete
{
    public class CommentRepository : ICommentRepository
    {
        private DNADbContext dbContext;
        public IMapper _mapper;

        public CommentRepository(DNADbContext context, IMapper mapper)
        {
            dbContext = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Creating new comment on database
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task<bool> Create(CreateCommentDto comment)
        {
            await dbContext.Comments.AddAsync(_mapper.Map<Comment>(comment));
            var isCreated = await dbContext.SaveChangesAsync();

            return isCreated > 0;
        }

        /// <summary>
        /// Delete comment from database by id
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        public async Task<bool> Delete(DeleteCommentDto post)
        {
            var currentComment = dbContext.Comments.Where(i => i.Id == post.Id).FirstOrDefault();
            if (currentComment is null)
                return false;

            dbContext.Remove(currentComment);

            var isDeleted = await dbContext.SaveChangesAsync();

            return isDeleted > 0;
        }

        /// <summary>
        /// Update comment by id
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        public async Task<bool> Update(UpdateCommentDto post)
        {
            var currentComment = dbContext.Comments.Where(i => i.Id == post.Id).FirstOrDefault();
            if (currentComment is null)
                return false;

            currentComment.PostId = post.PostId;
            currentComment.Status = post.Status;
            currentComment.Title = post.Title;
            currentComment.Content = post.Content;
            currentComment.SymbolId = post.SymbolId;
            currentComment.Image = post.Image;

            var isUpdated = await dbContext.SaveChangesAsync();

            return isUpdated > 0;
        }
        
        /// <summary>
        /// Getting all comments
        /// </summary>
        /// <returns></returns>
        public List<Comment> AllComments()
        {
            return dbContext.Comments.ToList();
        }

        /// <summary>
        /// Get comment by id
        /// </summary>
        /// <param name="commentId"></param>
        /// <returns></returns>
        public Comment CommentById(Guid commentId)
        {
            return dbContext.Comments.Where(i => i.Id == commentId).FirstOrDefault();
        }

        /// <summary>
        /// Set point for comment
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public async Task<bool> SetPoint(SetPointDto point)
        {
            var currentComment = dbContext.Comments.Where(i => i.Id == point.CommentId).FirstOrDefault();
            if (currentComment is null)
                return false;

            currentComment.PointCount += 1;
            currentComment.Point = (currentComment.Point + point.Point) / currentComment.PointCount;

            await dbContext.CommentPoints.AddAsync(_mapper.Map<CommentPoint>(point));
            var isUpdated = await dbContext.SaveChangesAsync();

            return isUpdated > 0;
        }

        /// <summary>
        /// Checking for is it pointed before
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="commentId"></param>
        /// <returns></returns>
        public bool IsPointedBefore(Guid userId, Guid commentId)
        {
            var isPointed = dbContext.CommentPoints.Where(i => i.CommentId == commentId && i.UserId == userId).FirstOrDefault();
            
            return isPointed != null;
        }
        
        /// <summary>
        /// Checking for is pinned before
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="commentId"></param>
        /// <returns></returns>
        public bool IsPinnedBefore(Guid userId, Guid commentId)
        {
            var isPointed = dbContext.PinnedComments.Where(i => i.CommentId == commentId && i.UserId == userId).FirstOrDefault();

            return isPointed != null;
        }

        /// <summary>
        /// Pinned a comment on db side
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task<bool> PinComment(PinCommentDto comment)
        {
            await dbContext.PinnedComments.AddAsync(_mapper.Map<PinnedComment>(comment));
            var isCreated = await dbContext.SaveChangesAsync();

            return isCreated > 0;
        }

        /// <summary>
        /// Pinned a comment on db side
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task<bool> UnpinComment(PinCommentDto comment)
        {
            var currentComment = dbContext.PinnedComments.Where(i => i.UserId == comment.UserId && i.CommentId == comment.CommentId).FirstOrDefault();

            if(currentComment is null)
                return false;

            dbContext.Remove(currentComment);

            var isDeleted = await dbContext.SaveChangesAsync();

            return isDeleted > 0;
        }

        /// <summary>
        /// Getting all pinned comments by user
        /// </summary>
        /// <returns></returns>
        public List<Comment> PinnedComments(Guid userId)
        {
            var pinnedComments = dbContext.PinnedComments.Where(i => i.UserId == userId)
                                                         .Join(dbContext.Comments,
                                                            pinnedComments => pinnedComments.CommentId,
                                                            comments => comments.Id,
                                                            (pinnedComments, comments) => new Comment
                                                            {
                                                                Id = comments.Id,
                                                                Content = comments.Content,
                                                                CreatedDate = comments.CreatedDate,
                                                                Image = comments.Image,
                                                                Point = comments.Point,
                                                                Status = comments.Status,
                                                                Title = comments.Title
                                                            }).ToList();

            return pinnedComments;
        }

    }
}
