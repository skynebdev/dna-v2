﻿using DNA.Domain.Dtos.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Repository.Identity.Abstract
{
    public interface IIdentityRepository
    {
        Task<bool> Register(RegisterDto register);
        Task<UserLoginInfoDto> Login(LoginDto login);
        bool IsEmailUsed(string email);
        bool IsMobileUsed(string mobile);

    }
}
