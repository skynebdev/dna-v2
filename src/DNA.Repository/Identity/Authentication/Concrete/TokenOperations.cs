﻿using DNA.EntityFrameworkCore.Context;
using DNA.Repository.Identity.Authentication.Abstract;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Repository.Identity.Authentication.Concrete
{
    public class TokenOperations : ITokenOperations
    {
        private readonly IConfiguration _configuration;
        private readonly DNADbContext dbContext;

        public TokenOperations(IConfiguration configuration, DNADbContext context)
        {
            _configuration = configuration;
            dbContext = context;
        }

        /// <summary>
        /// Generating new JWT Token
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public string CreateJwtToken(string email)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:SecurityKey"]));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expireDate = DateTime.Now.AddDays(int.Parse(_configuration["Jwt:ExpiryInDays"].ToString()));

            var claims = new[]
            {
                new Claim(ClaimTypes.Email,email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var tokenCreate = new JwtSecurityToken(_configuration["Jwt:Issuer"], _configuration["Jwt:Audience"], claims, null, expireDate, credentials);
            var token = new JwtSecurityTokenHandler().WriteToken(tokenCreate);

            return token;
        }

        /// <summary>
        /// Checking current token is match with user's token
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public bool IsTrueToken(Guid userId, string token, bool type = true)
        {
            dynamic user = null;

            if(type)
                user = dbContext.Users.Where(i => i.Id == userId).FirstOrDefault();
            else
                user = dbContext.Admins.Where(i => i.Id == userId).FirstOrDefault();

            if (user is null) { return false; }
            if (string.IsNullOrEmpty(user.Token)) { return false; }

            try { token = token.Split("Bearer ")[1]; } catch{ return false; }
            
            if (user.Token != token || DateTime.Now > user.TokenExpireDate) { return false; }

            return true;
        }

        /// <summary>
        /// Its saving token for user by email
        /// </summary>
        /// <param name="email"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<bool> SaveToken(string email, string token, bool type = true)
        {
            dynamic user = null;

            if(type)
                user = dbContext.Users.Where(i => i.Email == email).FirstOrDefault();
            else
                user = dbContext.Admins.Where(i => i.Email == email).FirstOrDefault();

            if (user is null)
                return false;

            user.Token = token;
            user.TokenExpireDate = DateTime.Now.AddHours(2);

            var isUpdated = await dbContext.SaveChangesAsync();

            return isUpdated > 0;
        }
    }
}
