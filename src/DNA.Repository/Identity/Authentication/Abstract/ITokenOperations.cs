﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Repository.Identity.Authentication.Abstract
{
    public interface ITokenOperations
    {
        string CreateJwtToken(string email);

        bool IsTrueToken(Guid userId, string token, bool type);

        Task<bool> SaveToken(string email, string token, bool type=true);

    }
}
