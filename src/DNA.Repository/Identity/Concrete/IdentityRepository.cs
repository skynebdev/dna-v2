﻿using AutoMapper;
using DNA.Core.Helpers;
using DNA.Domain.Dtos.Identity;
using DNA.Domain.Models;
using DNA.EntityFrameworkCore.Context;
using DNA.Repository.Identity.Abstract;
using DNA.Repository.Identity.Authentication.Abstract;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Repository.Identity.Concrete
{
    public class IdentityRepository : IIdentityRepository
    {
        private DNADbContext dbContext;
        public IMapper _mapper;
        public IConfiguration _configuration;
        private ITokenOperations _tokenOperations;

        public IdentityRepository(DNADbContext context, IMapper mapper, IConfiguration configuration, ITokenOperations tokenOperations)
        {
            dbContext = context;
            _mapper = mapper;
            _configuration = configuration;
            _tokenOperations = tokenOperations;
        }

        /// <summary>
        /// Using for record new user to database
        /// </summary>
        /// <param name="register"></param>
        /// <returns></returns>
        public async Task<bool> Register(RegisterDto register)
        {
            await dbContext.Users.AddAsync(_mapper.Map<User>(register));
            var isCreated = await dbContext.SaveChangesAsync();

            return isCreated > 0;
        }

        /// <summary>
        /// Checking database for login (email and password must be match)
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public async Task<UserLoginInfoDto> Login(LoginDto login)
        {
            dynamic user = null;
            dynamic saveToken;

            if (login.Type == Domain.Enums.Identity.UserTypes.User)
            {
                user = dbContext.Users.Where(i => i.Email == login.Email)
                                      .Where(i => i.Password ==  CoreHelpers.MD5Hash(login.Password)).FirstOrDefault();
            }
            else
            {
                user = dbContext.Admins.Where(i => i.Email == login.Email)
                                      .Where(i => i.Password ==  CoreHelpers.MD5Hash(login.Password)).FirstOrDefault();
            }

            if (user is null)
                return null;

            user.Token = _tokenOperations.CreateJwtToken(user.Email); //Create a new jwt token

            if (login.Type == Domain.Enums.Identity.UserTypes.User)
                saveToken = await _tokenOperations.SaveToken(user.Email, user.Token); //Save a jwt token for current user
            else
                saveToken = await _tokenOperations.SaveToken(user.Email, user.Token,false); //Save a jwt token for current user

            return _mapper.Map<UserLoginInfoDto>(user);
        }

        /// <summary>
        /// Looking for is email used
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool IsEmailUsed(string email)
        {
            var emailCheck = dbContext.Users.Where(u => u.Email == email).FirstOrDefault();

            if (emailCheck is not null)
                return true;

            return false;
        }

        /// <summary>
        /// Looking for is mobile used
        /// </summary>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public bool IsMobileUsed(string mobile)
        {
            var mobileCheck = dbContext.Users.Where(u => u.Mobile == mobile).FirstOrDefault();

            if (mobileCheck is not null)
                return true;

            return false;
        }
    }
}
