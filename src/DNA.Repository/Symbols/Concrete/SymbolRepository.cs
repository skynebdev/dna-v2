﻿using AutoMapper;
using DNA.Domain.Dtos.Symbols;
using DNA.Domain.Models.Symbols;
using DNA.EntityFrameworkCore.Context;
using DNA.Repository.Symbols.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Repository.Symbols.Concrete
{
    public class SymbolRepository : ISymbolRepository
    {
        private DNADbContext dbContext;
        public IMapper _mapper;

        public SymbolRepository(DNADbContext context, IMapper mapper)
        {
            dbContext = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Creating new symbol on database
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        public async Task<bool> Create(CreateSymbolDto symbol)
        {
            await dbContext.Symbols.AddAsync(_mapper.Map<Symbol>(symbol));
            var isCreated = await dbContext.SaveChangesAsync();

            return isCreated > 0;
        }

        /// <summary>
        /// Updatingsymbol on database
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        public async Task<bool> Update(UpdateSymbolDto symbol)
        {
            var currentSymbol = dbContext.Symbols.Where(i => i.Id == symbol.Id).FirstOrDefault();
            if(currentSymbol is null)
                return false;

            currentSymbol.Source = symbol.Source;            
            currentSymbol.Name = symbol.Name;            
            currentSymbol.Description = symbol.Description;            

            var isUpdated = await dbContext.SaveChangesAsync();

            return isUpdated > 0;
        }

        /// <summary>
        /// Delete symbol from database
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        public async Task<bool> Delete(DeleteSymbolDto symbol)
        {
            var currentSymbol = dbContext.Symbols.Where(i => i.Id == symbol.Id).FirstOrDefault();
            if (currentSymbol is null)
                return false;

            dbContext.Symbols.Remove(currentSymbol);
            var isDeleted = await dbContext.SaveChangesAsync();

            return isDeleted > 0;
        }

        /// <summary>
        /// Returned all symbols at database
        /// </summary>
        /// <returns></returns>
        public List<Symbol> AllSymbols()
        {
            return dbContext.Symbols.ToList();
        }
    }
}
