﻿using DNA.Domain.Dtos.Symbols;
using DNA.Domain.Models.Symbols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Repository.Symbols.Abstract
{
    public interface ISymbolRepository
    {
        Task<bool> Create(CreateSymbolDto symbol);
        Task<bool> Update(UpdateSymbolDto symbol);
        Task<bool> Delete(DeleteSymbolDto symbol);
        List<Symbol> AllSymbols();

    }
}
