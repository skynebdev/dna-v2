﻿using DNA.Domain.Dtos.Posts;
using DNA.Domain.Models.Posts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Repository.Posts.Abstract
{
    public interface IPostRepository
    {
        Task<bool> Create(CreatePostDto post);
        Task<bool> Update(UpdatePostDto post);
        Task<bool> Delete(DeletePostDto post);
        List<Post> AllPosts();
        Post PostById(Guid postId);
        Task IncreaseViews(Guid postId);
        bool IsPinnedBefore(Guid userId, Guid postId);
        Task<bool> PinPost(PinPostDto post);
        Task<bool> UnpinPost(PinPostDto post);
        List<Post> PinnedPosts(Guid userId);
    }
}
