﻿using AutoMapper;
using DNA.Domain.Dtos.Posts;
using DNA.Domain.Models.Posts;
using DNA.EntityFrameworkCore.Context;
using DNA.Repository.Posts.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Repository.Posts.Concrete
{
    public class PostRepository : IPostRepository
    {
        private DNADbContext dbContext;
        public IMapper _mapper;

        public PostRepository(DNADbContext context, IMapper mapper)
        {
            dbContext = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Creating new post on database
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        public async Task<bool> Create(CreatePostDto post)
        {
            await dbContext.Posts.AddAsync(_mapper.Map<Post>(post));
            var isCreated = await dbContext.SaveChangesAsync();

            return isCreated > 0;
        }

        /// <summary>
        /// Updating post on database
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        public async Task<bool> Update(UpdatePostDto post)
        {
            var currentPost = dbContext.Posts.Where(i => i.Id == post.Id).FirstOrDefault();
            if (currentPost is null)
                return false;

            currentPost.Source = post.Source;
            currentPost.Status = post.Status;
            currentPost.Title = post.Title;
            currentPost.Content = post.Content;
            currentPost.SymbolId = post.SymbolId;

            var isUpdated = await dbContext.SaveChangesAsync();

            return isUpdated > 0;
        }

        /// <summary>
        /// Delete post on database
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        public async Task<bool> Delete(DeletePostDto post)
        {
            var currentPost = dbContext.Posts.Where(i => i.Id == post.Id).FirstOrDefault();
            if (currentPost is null)
                return false;

            dbContext.Remove(currentPost);

            var isDeleted = await dbContext.SaveChangesAsync();

            return isDeleted > 0;
        }

        /// <summary>
        /// Getting all symbols from database
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public List<Post> AllPosts()
        {
            return dbContext.Posts.ToList();
        }
        
        /// <summary>
        /// Get a post from database by id
        /// </summary>
        /// <param name="postId"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Post PostById(Guid postId)
        {
            return dbContext.Posts.Where(i => i.Id == postId).FirstOrDefault();
        }

        /// <summary>
        /// Increase post view on click
        /// </summary>
        /// <param name="postId"></param>
        /// <returns></returns>
        public async Task IncreaseViews(Guid postId)
        {
            var currentViewers = dbContext.Posts.Where(i => i.Id == postId).FirstOrDefault();
            currentViewers.Viewers += 1;

            await dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Checking for is pinned before
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="postId"></param>
        /// <returns></returns>
        public bool IsPinnedBefore(Guid userId, Guid postId)
        {
            var isPointed = dbContext.PinnedPosts.Where(i => i.PostId == postId && i.UserId == userId).FirstOrDefault();

            return isPointed != null;
        }

        /// <summary>
        /// Pinned a Post on db side
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task<bool> PinPost(PinPostDto post)
        {
            await dbContext.PinnedPosts.AddAsync(_mapper.Map<PinnedPost>(post));
            var isCreated = await dbContext.SaveChangesAsync();

            return isCreated > 0;
        }

        /// <summary>
        /// Unpinned a post on db side
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task<bool> UnpinPost(PinPostDto post)
        {
            var currentPost = dbContext.PinnedPosts.Where(i => i.UserId == post.UserId && i.PostId == post.PostId).FirstOrDefault();

            if (currentPost is null)
                return false;

            dbContext.Remove(currentPost);

            var isDeleted = await dbContext.SaveChangesAsync();

            return isDeleted > 0;
        }

        /// <summary>
        /// Getting all pinned comments by user
        /// </summary>
        /// <returns></returns>
        public List<Post> PinnedPosts(Guid userId)
        {
            var pinnedPosts = dbContext.PinnedPosts.Where(i => i.UserId == userId)
                                                   .Join(dbContext.Posts,
                                                    pinnedPosts => pinnedPosts.PostId,
                                                    posts => posts.Id,
                                                    (pinnedPosts, posts) => new Post
                                                    {
                                                        Id = posts.Id,
                                                        Content = posts.Content,
                                                        CreatedDate = posts.CreatedDate,
                                                        Source = posts.Source,
                                                        Viewers = posts.Viewers,
                                                        Status = posts.Status,
                                                        Title = posts.Title
                                                    }).ToList();

            return pinnedPosts;
        }
    }
}
