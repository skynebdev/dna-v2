﻿using AutoMapper;
using DNA.Domain.Dtos.Products;
using DNA.Domain.Models.Products;
using DNA.EntityFrameworkCore.Context;
using DNA.Repository.Products.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Repository.Products.Concrete
{
    public class ProductRepository : IProductRepository
    {
        private DNADbContext dbContext;
        public IMapper _mapper;

        public ProductRepository(DNADbContext context, IMapper mapper)
        {
            dbContext = context;
            _mapper = mapper;
        }

        public async Task<bool> Create(CreateProductDto product)
        {
            await dbContext.Products.AddAsync(_mapper.Map<Product>(product));
            var isCreated = await dbContext.SaveChangesAsync();

            return isCreated > 0;
        }
        public async Task<bool> Update(UpdateProductDto product)
        {
            var currentProduct = dbContext.Products.Where(i => i.Id == product.Id).FirstOrDefault();
            if (currentProduct is null)
                return false;

            currentProduct.Period = product.Period;
            currentProduct.Price = product.Price;
            currentProduct.Name = product.Name;
            currentProduct.Description = product.Description;
            currentProduct.Type = product.Type;

            var isUpdated = await dbContext.SaveChangesAsync();

            return isUpdated > 0;
        }

        public async Task<bool> Delete(DeleteProductDto product)
        {
            var currentComment = dbContext.Products.Where(i => i.Id == product.Id).FirstOrDefault();
            if (currentComment is null)
                return false;

            dbContext.Remove(currentComment);

            var isDeleted = await dbContext.SaveChangesAsync();

            return isDeleted > 0;
        }

        public Product ProductById(Guid productId)
        {
            return dbContext.Products.Where(i => i.Id == productId).FirstOrDefault();
        }

        public List<Product> AllProducts()
        {
            return dbContext.Products.ToList();
        }
     
    }
}
