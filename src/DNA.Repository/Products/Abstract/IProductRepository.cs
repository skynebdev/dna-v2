﻿using DNA.Domain.Dtos.Products;
using DNA.Domain.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Repository.Products.Abstract
{
    public interface IProductRepository
    {
        Task<bool> Create(CreateProductDto product);
        Task<bool> Update(UpdateProductDto product);
        Task<bool> Delete(DeleteProductDto product);
        List<Product> AllProducts();
        Product ProductById(Guid productId);

    }
}
