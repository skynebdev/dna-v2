﻿using AutoMapper;
using DNA.Domain.Dtos.ClientProducts;
using DNA.Domain.Enums.ClientProducts;
using DNA.Domain.Enums.Products;
using DNA.Domain.Models.ClientProducts;
using DNA.EntityFrameworkCore.Context;
using DNA.Repository.ClientProducts.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Repository.ClientProducts.Concrete
{
    public class ClientProductsRepository : IClientProductsRepository
    {
        private DNADbContext dbContext;
        public IMapper _mapper;

        public ClientProductsRepository(DNADbContext context, IMapper mapper)
        {
            dbContext = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Creating new client product on database
        /// </summary>
        /// <param name="clientProducts"></param>
        /// <returns></returns>
        public async Task<bool> Create(CreateClientProductDto clientProducts)
        {
            await dbContext.ClientProducts.AddAsync(_mapper.Map<ClientProduct>(clientProducts));
            var isCreated = await dbContext.SaveChangesAsync();

            return isCreated > 0;
        }

        /// <summary>
        /// Get product period by product id
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public int GetProductPeriod(Guid productId)
        {
            var product = dbContext.Products.Where(i=>i.Id == productId).FirstOrDefault();

            if (product is null)
                return 0;

            return product.Period;
        }

        /// <summary>
        /// If client buyed product return expire date if not buyed return current date time
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public DateTime GetClientProductExpireDate(Guid productId, Guid userId)
        {
            var clientProduct = dbContext.ClientProducts.Where(i => i.UserId == userId && i.ProductId == productId).FirstOrDefault();

            if (clientProduct is not null)
                return clientProduct.ExpiredDate;

            return DateTime.Now;
        }

        /// <summary>
        /// Set user a new and active product
        /// </summary>
        /// <param name="clientProducts"></param>
        /// <returns></returns>
        public async Task<bool> SetProductToCustomer(SetProductToCustomerDto clientProducts)
        {
            var currentProduct = dbContext.ClientProducts.Where(i => i.Id == clientProducts.Id).FirstOrDefault();

            currentProduct.Status = clientProducts.Status;
            currentProduct.ExpiredDate = clientProducts.ExpiredDate;

            var isUpdated = await dbContext.SaveChangesAsync();

            return isUpdated > 0;
        }

        /// <summary>
        /// Checking for is there already pending request
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool IsThereAlreadyPendingRequest(Guid productId, Guid userId)
        {
            var isHaveProduct = dbContext.ClientProducts.Where(i => i.ProductId == productId)
                                                        .Where(i => i.UserId == userId)
                                                        .Where(i => i.Status == ProductStatuses.Pending).FirstOrDefault();

            return isHaveProduct != null;
        }

        /// <summary>
        /// Returned all product pending users
        /// </summary>
        /// <returns></returns>
        public List<PendingApprovalsDto> GetUsersPendingApproval()
        {
            var pendingProducts = (from clientProducts in dbContext.ClientProducts
                                   join products in dbContext.Products on clientProducts.ProductId equals products.Id
                                   join users in dbContext.Users on clientProducts.UserId equals users.Id
                                   where clientProducts.Status == ProductStatuses.Pending
                                   select new PendingApprovalsDto
                                   {
                                       Id = clientProducts.Id,
                                       ProductId = clientProducts.ProductId,
                                       UserId = users.Id,
                                       ProductName = products.Name,
                                       Description = products.Description,
                                       Price = products.Price,
                                       Period = products.Period,
                                       Type = products.Type,
                                       UserName = users.Name,
                                       Lastname = users.Lastname,
                                       Email = users.Email,
                                       Mobile = users.Mobile
                                   }).ToList();

            return pendingProducts;
        }

        /// <summary>
        /// Returned client products by client ıd
        /// </summary>
        /// <returns></returns>
        public ClientProductByClientDto GetClientProducts(Guid userId)
        {
            var productList = dbContext.ClientProducts.Where(i=> i.UserId == userId).Join(dbContext.Products,
                                                            clientProducts => clientProducts.ProductId,
                                                            products => products.Id,
                                                            (clientProducts, products) => new ProductList
                                                            {
                                                                Id = products.Id,
                                                                Description = products.Description,
                                                                ProductName = products.Name,
                                                                Price = products.Price,
                                                                Period = products.Period,
                                                                Type = products.Type,
                                                                Status = clientProducts.Status
                                                            }).ToList();
            
            var clientAllProducts = (from clientProducts in dbContext.ClientProducts
                                     join products in dbContext.Products on clientProducts.ProductId equals products.Id
                                     join users in dbContext.Users on clientProducts.UserId equals users.Id
                                     where users.Id == userId
                                     select new ClientProductByClientDto
                                     {
                                         Products = productList,
                                         UserId = users.Id,
                                         UserName = users.Name,
                                         Lastname = users.Lastname,
                                         Email = users.Email,
                                         Mobile = users.Mobile
                                     }).FirstOrDefault();

            return clientAllProducts;
        }

        /// <summary>
        /// Returned clients all products
        /// </summary>
        /// <returns></returns>
        public List<ClientAllProductsDto> GetAllClientProducts()
        {
            var clientAllProducts = (from clientProducts in dbContext.ClientProducts
                                     join products in dbContext.Products on clientProducts.ProductId equals products.Id
                                     join users in dbContext.Users on clientProducts.UserId equals users.Id
                                     select new ClientAllProductsDto
                                     {
                                         Id = clientProducts.Id,
                                         ProductDescription = products.Description,
                                         ProductName = products.Name,
                                         ProductPeriod = products.Period,
                                         ProductPrice = products.Price,
                                         ProductType = products.Type,
                                         Name = users.Name,
                                         Lastname = users.Lastname,
                                         Email = users.Email,
                                         Mobile = users.Mobile,
                                         Status = clientProducts.Status
                                     }).ToList();


            return clientAllProducts;
        }

        /// <summary>
        /// Is user used free trial or not
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool IsFreeTrialUsed(Guid userId)
        {
            var freeTrial = dbContext.ClientProducts.Join(dbContext.Products,
                                                        clientProducts => clientProducts.ProductId,
                                                        product => product.Id,
                                                        (clientProducts, product) => new
                                                        {
                                                            Type = product.Type,
                                                            UserId = clientProducts.UserId
                                                        }).Where(i => i.UserId == userId).ToList();

            if (freeTrial is not null)
            {
                for (int i = 0; i < freeTrial.Count; i++)
                {
                    if (freeTrial[i].Type == ProductTypes.Trial)
                        return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Set new free trial product to user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<bool> SetFreeTrial(Guid userId)
        {
            CreateClientProductDto clientProduct = new();
            clientProduct.UserId = userId;
            clientProduct.ProductId = GetFreeTrialProductId();
            clientProduct.ExpiredDate = DateTime.Now.AddDays(7);
            clientProduct.Status = ProductStatuses.Active;

            await dbContext.ClientProducts.AddAsync(_mapper.Map<ClientProduct>(clientProduct));
            var isCreated = await dbContext.SaveChangesAsync();

            return isCreated > 0;
        }

        /// <summary>
        /// Returned free trial product id
        /// </summary>
        /// <returns></returns>
        public Guid GetFreeTrialProductId()
        {
            return dbContext.Products.Where(i => i.Type == ProductTypes.Trial).FirstOrDefault().Id;
        }

        /// <summary>
        /// Checking for product is active or not
        /// </summary>
        /// <param name="postOrComment">True => POST , False => Comments</param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool IsProductBuyed(Guid userId, bool postOrComment)
        {
            dynamic product = null;

            if (postOrComment)
            {
                product = dbContext.ClientProducts.Where(i => i.UserId == userId && i.ExpiredDate > DateTime.Now)
                                                  .OrderByDescending( i=> i.ExpiredDate)
                                                  .Join(dbContext.Products,
                                                        clientProducts => clientProducts.ProductId,
                                                        products => products.Id,
                                                        (clientProducts, products) => new ProductList
                                                        {
                                                            Id = products.Id,
                                                            Description = products.Description,
                                                            ProductName = products.Name,
                                                            Price = products.Price,
                                                            Period = products.Period,
                                                            Type = products.Type,
                                                            Status = clientProducts.Status
                                                        }).FirstOrDefault(i => i.Type == ProductTypes.Standart || i.Type == ProductTypes.Premium || i.Type == ProductTypes.Trial);
            }
            else
            {
                product = dbContext.ClientProducts.Where(i => i.UserId == userId && i.ExpiredDate > DateTime.Now)
                                                  .OrderByDescending(i => i.ExpiredDate)
                                                  .Join(dbContext.Products,
                                                        clientProducts => clientProducts.ProductId,
                                                        products => products.Id,
                                                        (clientProducts, products) => new ProductList
                                                        {
                                                            Id = products.Id,
                                                            Description = products.Description,
                                                            ProductName = products.Name,
                                                            Price = products.Price,
                                                            Period = products.Period,
                                                            Type = products.Type,
                                                            Status = clientProducts.Status
                                                        }).FirstOrDefault(i => i.Type == ProductTypes.Premium);
            }

            if (product is not null)
                return true;

            return false;

        }
    }
}
