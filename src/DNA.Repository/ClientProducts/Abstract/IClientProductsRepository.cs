﻿using DNA.Domain.Dtos.ClientProducts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNA.Repository.ClientProducts.Abstract
{
    public interface IClientProductsRepository
    {
        Task<bool> Create(CreateClientProductDto clientProducts);
        int GetProductPeriod(Guid productId);
        DateTime GetClientProductExpireDate(Guid productId, Guid userId);
        Task<bool> SetProductToCustomer(SetProductToCustomerDto clientProducts);
        bool IsThereAlreadyPendingRequest(Guid productId, Guid userId);
        List<PendingApprovalsDto> GetUsersPendingApproval();
        ClientProductByClientDto GetClientProducts(Guid userId);
        List<ClientAllProductsDto> GetAllClientProducts();
        bool IsFreeTrialUsed(Guid userId);
        Task<bool> SetFreeTrial(Guid userId);
        bool IsProductBuyed(Guid userId, bool postOrComment);
    }
}
